#include "estacion.h"
#include "ui_estacion.h"

Estacion::Estacion(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Estacion)
{
    ui->setupUi(this);

    this->mSrvClienteWeb=new miServer;
    connect(this->mSrvClienteWeb,SIGNAL(sgnMensajeConSocket(QByteArray,QTcpSocket*)),this,SLOT(sltMensajeDesdeWeb(QByteArray,QTcpSocket*)));
    this->mSrvClienteWeb->listen(QHostAddress::LocalHost,PUERTO_BASES_ORDEN_WEB);

    this->mSrvReservaBase=new miServer;
    connect(this->mSrvReservaBase,SIGNAL(sgnMensajeConSocket(QByteArray,QTcpSocket*)),this,SLOT(sltPeticionReservaBase(QByteArray,QTcpSocket*)));
    this->mSrvReservaBase->listen(QHostAddress::LocalHost,PUERTO_PETICION_RESERVA_PARA_BASE);

    this->basActual=0;

    this->mSem=new CSemaforo("/dev/ttyS5");
    this->mSem->start();

    this->iniciarHilo();
    if(!iniciarPuerto())
    {
        qDebug()<<"No se ha podido iniciar el puerto...";

    }
    else if(this->obtenerConfiguracion())
    {
        qDebug()<<"Cargada configuracion correctamente.";
        if(this->iniciarBD())
        {
            if(this->mBases.count()>0)
            {
                this->cargarMensajesPendientes();


                qDebug()<<"Bases cargadas correctamente.";
                this->mPooling=new QTimer;
                connect(this->mPooling,SIGNAL(timeout()),this,SLOT(sltPooling()));
                this->posBasActual=0;

                this->mPooling->start(10);

                this->mTimerEnvioMensajesEnganchesYdesenganches=new QTimer;
                connect(this->mTimerEnvioMensajesEnganchesYdesenganches,SIGNAL(timeout()),this,SLOT(sltEnvioMensajesEngancheYdesenganche()));
                this->mTimerEnvioMensajesEnganchesYdesenganches->start(200);


                this->mTimerEnvioMensajesEstados=new QTimer;
                connect(this->mTimerEnvioMensajesEstados,SIGNAL(timeout()),this,SLOT(sltEnvioMensajesEstados()));
                this->mTimerEnvioMensajesEstados->start(200);

                this->mTimerInfoUnaBase=new QTimer;
                connect(this->mTimerInfoUnaBase,SIGNAL(timeout()),SLOT(sltEnviarEstadoDeUnaBase()));
                this->mTimerInfoUnaBase->start(30000);


            }
            else
            {
                qDebug()<<"Error al cargar las bases...";
            }
        }
        else
        {
            qDebug()<<"Error al cargar la base de datos...";
        }
    }

}
void Estacion::encederRojo()
{

    qDebug()<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
    this->mSem->setAccion(SEMAFORO_ENCENDER_ROJO);
    /*

    //this->sem->apagarRele1();
    qDebug()<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
    //this->sem->encenderRele2();
    qDebug()<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");*/
}
void Estacion::encenderVerde()
{
     this->mSem->setAccion(SEMAFORO_ENCENDER_VERDE);
  //  this->sem->apagarRele2();
  //  this->sem->encenderRele1();
}

void Estacion::sltPeticionReservaBase(QByteArray msg, QTcpSocket *s)
{
    qDebug()<<"yeah!";
    bool respuesta=false;
    QString causa;
    QString mensaje_texto=msg.constData();
    QList<QString> mensaje=mensaje_texto.split('#');
     int basReserva=-1;
    if(mensaje.count()>3)
    {
        int comando=mensaje[0].toInt();
        switch(comando)
        {
            case 1:
            {

                int comando_reservas=mensaje[1].toInt();

                if(comando_reservas==1)
                {
                    int baseslibres=0;
                    int basesocupadas=0;

                    QString rfid=mensaje[2];
                    QString sentencia="select dni from vav_abono where idrfid='"+rfid+"'";
                    QSqlQuery q;
                    if(q.exec(sentencia))
                    {
                        if(q.next())
                        {
                            QString sentencia1="select estado,idbase from base where idestacion='"+QString::number(this->idEstacion)+"'";
                            QSqlQuery q1;
                            if(q1.exec(sentencia1))
                            {

                                while(q1.next())
                                {
                                    int estado=q1.value(0).toInt();
                                    int basActual=q1.value(1).toInt();
                                    if(estado==3)
                                    {
                                        basesocupadas++;
                                    }
                                    else if (estado==4)
                                    {
                                        if(basReserva==-1)
                                        {
                                            for(int u=0;u<this->mBases.count();u++)
                                            {
                                                if(basActual==this->mBases[u]->getId())
                                                {
                                                    QSqlQuery q1;
                                                    QString sentencia1="select * from reservas where idrfid='"+rfid+"' and estado=1";
                                                    if(q1.exec(sentencia1))
                                                    {
                                                        if(!q1.next())
                                                        {
                                                            respuesta=true;
                                                            basReserva=u;
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            causa="Ya tiene reserva previa.";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        qDebug()<<"Error en la sentencia:"<<sentencia1;
                                                    }


                                                }
                                            }

                                        }
                                        baseslibres++;
                                    }
                                }

                                if(basReserva!=1)
                                {

                                }
                                else if(basReserva==-1)
                                {
                                     causa="No hay bases libres";
                                }

                            }
                            else
                            {
                                qDebug()<<q1.lastError().text()<<"de la sentencia: "<<sentencia1;
                            }

                        }
                        else
                        {
                             causa="RFID incorrecto";
                        }

                    }
                    else
                    {
                        qDebug()<<q.lastError().text()<<"de la sentencia: "<<sentencia;
                    }
                    if(respuesta)
                    {
                        qDebug()<<"La peticion la envio a las:"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                        STR_RESERVA_BASE strReseva;
                        strReseva.idbase=basReserva;
                        strReseva.skt=s;
                        this->mStrReservas.append(strReseva);
                        //   this->agregarOrdenParaBase(this->mBases[basReserva]->recibirEstadoBase());
                        qDebug()<<"se supone que se guarda";

                        bool reservaPrevia=false;
                        QSqlQuery q;
                        QString sentencia="select count(*) from reservas where idrfid='"+rfid+"' and estado=1";
                        if(q.exec(sentencia))
                        {
                            if(q.next())
                            {
                                int c=q.value(0).toInt();
                                if(c>0)
                                {
                                   reservaPrevia=true;
                                }
                            }
                        }
                        else
                        {
                            qDebug()<<"Error en la sentencia:"<<sentencia;
                        }
                        if(!reservaPrevia)
                        {
                            int seg=60*60;
                            db.transaction();
                            sentencia="insert into reservas (idrfid,idestacion,idbase,fecha_fin) values('"+rfid+"',"+QString::number(this->idEstacion)+","+QString::number(this->mBases[basReserva]->getId())+",'"+QDateTime::currentDateTime().addSecs(seg).toString("yyyy/MM/dd HH:mm:ss")+"')";
                            if(q.exec(sentencia))
                            {
                                QByteArray respuesta_reserva("1#2#");
                                respuesta_reserva.append(QString::number(this->mBases[basReserva]->getId()));
                                respuesta_reserva.append("#");
                                respuesta_reserva.append(QDateTime::currentDateTime().addSecs(seg).toString("yyyy/MM/dd HH:mm:ss"));
                                respuesta_reserva.append("#");
                                if(s->state()==QTcpSocket::ConnectedState)
                                {
                                    s->write(respuesta_reserva);
                                    if(s->waitForBytesWritten(100))
                                    {
                                        db.commit();
                                        qDebug()<<"Mensaje escrito!";
                                        this->agregarOrdenParaBase(this->mBases[basReserva]->enviarOrden(ORDEN_BASE_RESERVADA));


                                    }
                                    else
                                    {
                                        db.rollback();
                                        qDebug()<<"No ha podido escribir";
                                    }
                                }
                            }
                            else
                            {
                                qDebug()<<"Error en la sentencia:"<<sentencia;
                            }
                        }
                        else
                        {
                            causa="Ya tiene una reserva anterior.";
                            QByteArray respuesta_reserva("1#3#");
                            respuesta_reserva.append(causa+"#");
                            s->write(respuesta_reserva);
                            if(s->waitForBytesWritten(100))
                            {
                                qDebug()<<"Mensaje escrito!";
                            }
                            else
                            {
                                qDebug()<<"No ha podido escribir";
                            }
                        }
/*

                     //   bool salida;
                         QEventLoop loop;
                         int ms=10000;
                         QTimer timer;

                         timer.setSingleShot(true);
                         connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
                         connect(this,SIGNAL(sgnReservaBase()),&loop,SLOT(quit()));


                         timer.start(ms); //your predefined timeout

                         loop.exec();

                         bool reservaPrevia=false;

                         if (timer.isActive()) //replay received before timer, you can then get replay form network access manager and do whatever you want with it
                         {
                             int seg=10*60;//10 minutos
                             QSqlQuery q;
                             QString sentencia="select count(*) from reservas where idrfid='"+rfid+"' and estado=1";
                             if(q.exec(sentencia))
                             {
                                 if(q.next())
                                 {
                                     int c=q.value(0).toInt();
                                     if(c>0)
                                     {
                                        reservaPrevia=true;
                                     }
                                 }
                             }
                             else
                             {
                                 qDebug()<<"Error en la sentencia:"<<sentencia;
                             }
                             if(!reservaPrevia)
                             {
                                 sentencia="insert into reservas (idrfid,idestacion,idbase,fecha_fin) values('"+rfid+"',"+QString::number(this->idEstacion)+","+QString::number(this->mBases[basReserva]->getId())+",'"+QDateTime::currentDateTime().addSecs(seg).toString("yyyy/MM/dd HH:mm:ss")+"')";
                                 if(q.exec(sentencia))
                                 {
                                     QByteArray respuesta_reserva("1#2#");
                                     if(s->state()==QTcpSocket::connected())
                                     {
                                         s->write(respuesta_reserva);
                                         if(s->waitForBytesWritten(100))
                                         {
                                             qDebug()<<"Mensaje escrito!";

                                         }
                                         else
                                         {
                                             qDebug()<<"No ha podido escribir";
                                         }
                                     }
                                 }
                                 else
                                 {
                                     qDebug()<<"Error en la sentencia:"<<sentencia;
                                 }
                             }
                             else
                             {
                                 causa="Ya tiene una reserva anterior.";
                                 QByteArray respuesta_reserva("1#3#");
                                 respuesta_reserva.append(causa+"#");
                                 s->write(respuesta_reserva);
                                 if(s->waitForBytesWritten(100))
                                 {
                                     qDebug()<<"Mensaje escrito!";
                                 }
                                 else
                                 {
                                     qDebug()<<"No ha podido escribir";
                                 }
                             }
                         }
                         else //timer elapsed, no replay from client, ups
                         {
                             this->agregarOrdenParaBase(this->mBases[basReserva]->enviarOrden(ORDEN_FIN_BASE_RESERVADA));
                             causa="Timeout";
                             QByteArray respuesta_reserva("1#3#");
                             respuesta_reserva.append(causa+"#");
                             s->write(respuesta_reserva);
                             if(s->waitForBytesWritten(100))
                             {
                                 qDebug()<<"Mensaje escrito!";
                             }
                             else
                             {
                                 qDebug()<<"No ha podido escribir";
                             }
                         }

*/
                    }
                    else
                    {
                        QByteArray respuesta_reserva("1#3#");
                        respuesta_reserva.append(causa+"#");
                        s->write(respuesta_reserva);
                        if(s->waitForBytesWritten(100))
                        {
                            qDebug()<<"Mensaje escrito!";
                        }
                        else
                        {
                            qDebug()<<"No ha podido escribir";
                        }
                    }
                }

                break;
            }
        }
    }



}
 void Estacion::sltMensajeDesdeWeb(QByteArray msg, QTcpSocket *s)
 {
     quint16 crc=CalcularCRC(msg,msg.count());
     if(crc!=0)
     {
         return;
     }
     if(msg.count()<5)return;
     int estacionH=msg[0];
     int estacionL=msg[1];
     int estacion=256*estacionH+estacionL;
     int idbase=msg[2];
     int bas=-1;
     for(int i=0;i<this->mBases.count();i++)
     {
         if(this->mBases[i]->getId()==idbase)
         {
             bas=i;
             break;
         }
     }
     if(bas==-1)
     {
         qDebug()<<"Error";
     }
     int cmd=msg[3];
     int c=msg[4];

     QByteArray datos=msg.mid(5,c);
     switch(cmd)
     {
     case COMANDO_WEB_BLOQUEAR_BASE:
     {
         bool v=(quint8(datos[0])==1)?true:false;
         this->mBases[bas]->setBloqueada(v);

         break;
     }// 0x01;
     case COMANDO_WEB_RESURRECCION         :
     {
         this->agregarOrdenParaBase(this->mBases[bas]->enviarOrden(ORDEN_RESET));

         break;
     }// 0x02;
     case COMANDO_WEB_PROGRAMAR_PARAMETRO  :
     {
         int param=datos[0];
         QByteArray valor=datos.mid(1,datos.count()-1);

         //this->agregarMensajesParaCircuito(this->misBases[bas]->programarParametro(param,valor));

         break;
     }// 0x03;
     case COMANDO_WEB_LIBERAR_BICI         :
     {

         qDebug()<<datos.toHex();
         UNI_ESTADO_BASE estadoActual= this->mBases[bas]->getUniEstado();
         int est=estadoActual.estructura.estado;
         STR_DESENGANCHE strDesenganche;
         strDesenganche.fecha_permiso=QDateTime::currentDateTime();
         for(int u=0;u<4;u++)
         {
             strDesenganche.idbici.append(estadoActual.estructura.idbici[u]);
         }
         for(int u=0;u<7;u++)
         {
             strDesenganche.idrfid.append(datos[u]);
         }
         strDesenganche.semaforo=this->getEstadoSemaforo(true);
         this->mBases[bas]->setDatosDesenganchePendiente(strDesenganche);

         if(est==ESTADO_BICI_ENGANCHADA)
         {
             this->agregarOrdenParaBase(this->mBases[bas]->enviarOrdenLiberarConParametros(this->idEstacion,strDesenganche.idrfid));
         }
         else
         {
             this->agregarOrdenParaBase(this->mBases[bas]->enviarOrden(ORDEN_LIBERAR_SIN_PARAMETROS));
         }
     break;
     }// 0x04;
     case COMANDO_WEB_CARGAR_BICI          :
     {
     break;
     }// 0x05;
     case COMANDO_WEB_PARAR_CARGA          :
         {
         break;
         }// 0x06;

     case COMANDO_WEB_RESTABLECER_COMUNICACION          :
         {
            this->mBases[bas]->setNumeroDeMensajesSinRespuesta(0);
             this->mBases[bas]->setBloqueada(false);
             this->enviarEstadoAServidor(bas);


         break;
         }// 0x06;
     }

 }

void Estacion::sltEnviarEstadoDeUnaBase()
{
    QByteArray msg=this->enviarEstadoAServidor(this->basActual);
    this->agregarMensajeAListaParaCliente(msg,false);
   // for(int i=0;i<this->mBases.count();i++)
   // {
   ////     QByteArray msg=this->enviarEstadoAServidor(i);
    //    this->agregarMensajeAListaParaCliente(msg,false);
   // }
    this->basActual++;
    if(this->basActual>=this->mBases.count())
    {
        this->basActual=0;
    }
}

void Estacion::cargarMensajesPendientes()
{
    QSqlQuery q;
    QString sentencia="select mensaje,tipo from pendientes";
    if(q.exec(sentencia))
    {
        while(q.next())
        {
            int tipo=q.value(1).toInt();
            QString str=q.value(0).toString();
            QByteArray data = QByteArray::fromHex(str.toLatin1());
            if(tipo==4)
            {
                this->listaParaClienteEnganchesYdesenganches.append(data);
            }


        }
    }
}

void Estacion::iniciarHilo()
{
    this->mTimerEnvioMensajesEnganchesYdesenganches=new QTimer;
    connect(this->mTimerEnvioMensajesEnganchesYdesenganches,SIGNAL(timeout()),this,SLOT(sltEnvioMensajesEngancheYdesenganche()));
    this->mTimerEnvioMensajesEnganchesYdesenganches->start(500);

    this->mTimerEnvioMensajesEstados=new QTimer;
    connect(this->mTimerEnvioMensajesEstados,SIGNAL(timeout()),this,SLOT(sltEnvioMensajesEstados()));
    this->mTimerEnvioMensajesEstados->start(500);
}

bool Estacion::iniciarPuerto()
{
    bool ok=true;
    std::string nombre="/dev/ttyS1";//ui->lineEditPuertoSerie->text().toUtf8().constData();
  //  std::string nombre="/dev/ttyUSB0";
    serial_port.Open( nombre) ;

    if ( ! serial_port.good() )
    {
        std::cerr << "[" << __FILE__ << ":" << __LINE__ << "] "
                  << "Error: Could not open serial port."
                  << std::endl ;
        //this->addToLog("Error abriendo el puerto serie");
        ok=false;
    }
    //
    // Set the baud rate of the serial port.
    //
    serial_port.SetBaudRate( SerialStreamBuf::BAUD_38400 ) ;

    //serial_port.SetBaudRate( SerialStreamBuf::BAUD_38400 ) ;
    if ( ! serial_port.good() )
    {
        std::cerr << "Error: Could not set the baud rate." << std::endl ;
       // this->addToLog("Error abriendo el puerto serie");
       ok=false;
    }
    //
    // Set the number of data bits.
    //
    serial_port.SetCharSize( SerialStreamBuf::CHAR_SIZE_8 ) ;
    if ( ! serial_port.good() )
    {
        std::cerr << "Error: Could not set the character size." << std::endl ;
       // this->addToLog("Error abriendo el puerto serie");
        ok=false;
    }
    //
    // Disable parity.
    //
    serial_port.SetParity( SerialStreamBuf::PARITY_NONE ) ;
    if ( ! serial_port.good() )
    {
        std::cerr << "Error: Could not disable the parity." << std::endl ;
      //  this->addToLog("Error abriendo el puerto serie");
        ok=false;
    }
    //
    // Set the number of stop bits.
    //
    serial_port.SetNumOfStopBits( 2 ) ;
    if ( ! serial_port.good() )
    {
        std::cerr << "Error: Could not set the number of stop bits."
                  << std::endl ;
        //this->addToLog("Error abriendo el puerto serie");
        ok=false;
    }
    //
    // Turn on hardware flow control.
    //
    serial_port.SetFlowControl( SerialStreamBuf::FLOW_CONTROL_NONE ) ;
    if ( ! serial_port.good() )
    {
        std::cerr << "Error: Could not use hardware flow control."
                  << std::endl ;
        //this->addToLog("Error abriendo el puerto serie");
        ok=false;
    }
    if(ok)
    {
        qDebug()<<"Puerto abierto!";
        //addToLog("PUERTO ABIERTO!");
        //this->posible=true;
        //ui->groupBox->setEnabled(true);

    }
    else
    {
        qDebug()<<"error!";
      //  this->posible=false;

    }
    return ok;
}

bool Estacion::obtenerConfiguracion()
{
    bool ok=false;
    QString texto="DENTRO DE LA FUNCION";
    QFile file("/home/bonopark/DESARROLLO/configuraciones/configuracionTotem.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<file.errorString();
        texto="Error al cargar el fichero de configuracion: "+file.errorString();

        //return;
    }
    else
    {
        texto="Cargado correctamente el fichero de configuracion";
        QList<QByteArray>dato;

        while (!file.atEnd()) {
            QByteArray line = file.readLine();
            dato.append(line.split('#'));
        }
        this->idEstacion=dato[0].toInt();
        ok=true;
       // this->mIP=dato[1];
    }
    return ok;
}

bool Estacion::iniciarBD()
{
    bool ok=false;
    QString salida="";

   if (QSqlDatabase::isDriverAvailable("QMYSQL"))
   {
       db = QSqlDatabase::addDatabase("QMYSQL");
       db.setHostName("localhost");
       db.setDatabaseName("neo_bonopark_madrid");
       db.setUserName("root");
       db.setPassword("g077um");
       db.setConnectOptions("MYSQL_OPT_RECONNECT=1");
       if ( db.open() )
       {
          salida="Bien, base de datos cargada";
          ok=true;
       }

       else
       {
           QSqlError mensaje = db.lastError();
           salida=mensaje.text();//.setText(mensaje.text());
       }
   }
   else salida="No hay driver";
   qDebug()<<salida;
   if(ok)
   {
       int bz=0;
       QString sentencia="select idbase,estado from base where idestacion="+QString::number(this->idEstacion);
       QSqlQuery q;
       if(q.exec(sentencia))
       {
           while(q.next())
           {
               int id=q.value(0).toInt();
               this->mBases.append(new CBase(id));
               int estado=q.value(1).toInt();
               if(estado==ESTADO_BASE_RESERVADA)
               {
                   QSqlQuery q1;
                   QString sentencia2="select estado from reservas where idestacion="+QString::number(this->idEstacion)+ " and idbase="+QString::number(this->mBases[bz]->getId());

                   if(q1.exec(sentencia2))
                   {
                       if(q1.next())
                       {
                           int estado1=q1.value(0).toInt();
                           if(estado1==1)
                           {
                               this->agregarOrdenParaBase(this->mBases[bz]->enviarOrden(ORDEN_BASE_RESERVADA));

                           }
                       }
                   }
               }
               bz++;
           }
       }
   }
   return ok;

}
void Estacion::borrarMensajePendienteEngancheYdesenganche(QByteArray msg)
{
    QSqlQuery q;
    QString sentencia="delete from pendientes where mensaje=\""+msg.toHex()+"\" and tipo=4";
    if(q.exec(sentencia))
    {
        for(int i=0;i<this->listaParaClienteEnganchesYdesenganches.count();i++)
        {
            if(this->listaParaClienteEnganchesYdesenganches.at(i)==msg)
            {
                this->listaParaClienteEnganchesYdesenganches.removeAt(i);
                break;
            }
        }
    }
    else
    {
        qDebug()<<"Error en la sentencia sql:"<<sentencia;
    }
}

void Estacion::borrarMensajePendienteEstados(QByteArray msg)
{
    for(int i=0;i<this->listaParaClienteEstados.count();i++)
    {
        if(this->listaParaClienteEstados.at(i)==msg)
        {
            this->listaParaClienteEstados.removeAt(i);
            break;
        }
    }
}

void Estacion::sltEnvioMensajesEstados()
{
    this->mTimerEnvioMensajesEstados->stop();

    if(this->listaParaClienteEstados.count()>0)
    {
        QByteArray msg=this->listaParaClienteEstados.first();

        SocketImanol sI;
        sI.connectToHost(QHostAddress::LocalHost,PUERTO_CLIENTE_BASES_ESTADOS);
        if(sI.esperarAconectar(200))
        {
            sI.write(msg);
            if(sI.esperarAescribir(200))
            {
                if(sI.esperarArecibirRespuesta(200))
                {
                    QByteArray respuesta=sI.readAll();
                    this->borrarMensajePendienteEstados(respuesta);
                   // qDebug()<<"Respuesta OK al estado!"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss");
                }
                else
                {
                    qDebug()<<"No ha respondido al estado:"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss");
                }
            }
        }
    }



    this->mTimerEnvioMensajesEstados->start(200);
}

void Estacion::sltEnvioMensajesEngancheYdesenganche()
{
    this->mTimerEnvioMensajesEnganchesYdesenganches->stop();

    if(this->listaParaClienteEnganchesYdesenganches.count()>0)
    {
        QByteArray msg=this->listaParaClienteEnganchesYdesenganches.first();

        SocketImanol sI;
        sI.connectToHost(QHostAddress::LocalHost,PUERTO_CLIENTE_BASES_ENGANCHES_DESENGANCHES);
        if(sI.esperarAconectar(200))
        {
            sI.write(msg);
            if(sI.esperarAescribir(200))
            {
                if(sI.esperarArecibirRespuesta(200))
                {
                    QByteArray respuesta=sI.readAll();
                    this->borrarMensajePendienteEngancheYdesenganche(respuesta);
                }
            }
        }
    }
    this->mTimerEnvioMensajesEnganchesYdesenganches->start(200);
}

void Estacion::sltPooling()
{
    this->mPooling->stop();//Paro el timer para procesar el pooling y no llegue uno nuevo antes de terminar de procesar

    QDateTime ahora=QDateTime::currentDateTime();

    int basEmpleando=-1;

    bool ok=false;
    QByteArray msg;
    bool esOrden=false;
    if(this->listaOrdenes.count()>0)
    {
        int idbas=this->listaOrdenes.first()[0];
        int bas=-1;
        for(int i=0;i<this->mBases.count();i++)
        {
            if(this->mBases[i]->getId()==idbas)
            {
                bas=i;
                break;
            }
        }
        if(bas!=-1)
        {
            basEmpleando=bas;
            if(!this->mBases[bas]->getBloqueada())
            {
                msg=this->listaOrdenes.first();
                esOrden=true;

                qDebug()<<"La orden:"<<msg.toHex();
            }
            else
            {
                this->listaOrdenes.removeFirst();
            }
        }
    }
    else
    {
        basEmpleando=posBasActual;
        if(!this->mBases[posBasActual]->getBloqueada())
        {
             msg=this->mBases[this->posBasActual]->recibirEstadoBase();
        }
    }
    if(msg.count()>0)
    {
        this->bufferCircularEnviados.append(msg);
        int basEnvio=msg[0];
        QByteArray rsp;

        rsp=this->escribirYleerRespuesta(msg,0,5000);



        if(rsp.count()>0)
        {
            this->bufferCircularRecibidos.append(rsp);
             int basRespuesta=rsp[0];
             quint16 crc=CalcularCRC(rsp,rsp.count());
             if(crc==0)
             {
                 ok=true;
                 this->procesarRespuesta(rsp);
                 if(esOrden)
                 {
                 //    qDebug()<<"rsp:"<<rsp.toHex();
                     this->listaOrdenes.removeFirst();
                 }
             }
             else
             {
                 qDebug()<<"Error de crc";
           //      qDebug()<<"Rsp:"<<rsp.toHex();
              //   numMensajesError++;
             }

             if(basRespuesta==basEnvio)
             {


             }
             else
             {
                 rsp=this->escribirYleerRespuesta(msg,0,2000);
                 qDebug()<<"esta respuesta no es mia.... INTENTO 1";


                 int basEnvio=msg[0];
                 QByteArray rsp;

                 rsp=this->escribirYleerRespuesta(msg,0,2000);



                 if(rsp.count()>0)
                 {
                     this->bufferCircularRecibidos.append(rsp);
                      int basRespuesta=rsp[0];
                      quint16 crc=CalcularCRC(rsp,rsp.count());
                      if(crc==0)
                      {
                          ok=true;
                          this->procesarRespuesta(rsp);
                          if(esOrden)
                          {
                          //    qDebug()<<"rsp:"<<rsp.toHex();
                              this->listaOrdenes.removeFirst();
                          }
                      }
                      else
                      {
                          qDebug()<<"Error de crc";
                    //      qDebug()<<"Rsp:"<<rsp.toHex();
                       //   numMensajesError++;
                      }

                      if(basRespuesta==basEnvio)
                      {


                      }
                      else
                      {
                          rsp=this->escribirYleerRespuesta(msg,0,2000);
                          qDebug()<<"esta respuesta no es mia.... PSSS";



                      }

                     //ui->labelUltimoRecibido->setText(rsp.toHex()+"->"+ahora.toString("yyyy/MM/dd HH:mm:ss:zzz"));

                   }
                 else
                 {
                     //numMensajesError++;
                 }



             }

            //ui->labelUltimoRecibido->setText(rsp.toHex()+"->"+ahora.toString("yyyy/MM/dd HH:mm:ss:zzz"));

          }
        else
        {
            //numMensajesError++;
        }
     //   ui->labelConError->setText(QString::number(numMensajesError));
     //   ui->labelMensajesEnviados->setText(QString::number(numMensajesEscritos));
        if(!ok)
        {
            qDebug()<<"la base "<<this->mBases[basEmpleando]->getId()<<" no responde...";
            int c=this->mBases[basEmpleando]->getNumeroDeMensajesSinRespuesta();
            this->mBases[basEmpleando]->setNumeroDeMensajesSinRespuesta(c+1);
            if(this->mBases[basEmpleando]->getBloqueada())
            {
                int idbasBloqueada=this->mBases[basEmpleando]->getId();
                QList<QByteArray> listaOrdenesTemporal;
                for(int u=0;u<this->listaOrdenes.count();u++)
                {
                    int idbas=this->listaOrdenes.at(u)[0];

                    if(idbasBloqueada!=idbas)
                    {
                        listaOrdenesTemporal.append(this->listaOrdenes.at(u));
                    }

                }
                this->listaOrdenes.clear();
                this->listaOrdenes.append(listaOrdenesTemporal);
                QByteArray msg=this->enviarEstadoAServidor(basEmpleando);
                this->agregarMensajeAListaParaCliente(msg,false);
                QSqlQuery q;
                QString sentencia="update base set estado="+QString::number(ESTADO_NO_COMUNICA)+" where idestacion="+QString::number(this->idEstacion)+" and idbase= "+QString::number(this->mBases[basEmpleando]->getId());
                if(q.exec(sentencia))
                {
                    qDebug()<<"Bloqueada la base.";
                }
                else
                {
                    qDebug()<<"error en la sentencia sql:"<<sentencia;
                }
            }
        }
    }


    if(ok||msg.count()==0)
    {
        this->posBasActual++;
        if(this->posBasActual>=this->mBases.count())
        {
            this->posBasActual=0;
        }
    }
/*27 BASES 20ms/base->*/

    if(this->bufferCircularEnviados.count()>10)this->bufferCircularEnviados.removeFirst();
    if(this->bufferCircularRecibidos.count()>10)this->bufferCircularRecibidos.removeFirst();
    int tim=20;//(26-this->mBases.count())*20;

    this->mPooling->start(tim);//Reinicio el timer.
}
QByteArray Estacion::escribirYleerRespuesta(QByteArray msg,int numRespuesta, int tiempomax)
{
    //numMensajesEscritos++;

    QByteArray salida;
    QDateTime ahora=QDateTime::currentDateTime();

  //  qDebug()<<"Escribo "<<msg.toHex()<<"->"<<ahora.toString("yyyy/MM/dd HH:mm:ss:zzz");
    //ui->labelUltimoEnviado->setText(msg.toHex()+"->"+ahora.toString("yyyy/MM/dd HH:mm:ss:zzz"));
    this->serial_port.write(msg.data(),msg.count());

    salida=this->leerRespuesta(tiempomax,numRespuesta);
 // qDebug()<<"Recibo "<<salida.toHex()<<"->"<<ahora.toString("yyyy/MM/dd HH:mm:ss:zzz");


   // ui->labelConError->setText(QString::number(numMensajesError));
   // ui->labelMensajesEnviados->setText(QString::number(numMensajesEscritos));
    return salida;
}

QByteArray Estacion::leerRespuesta(int tiempo, int numRespuesta)
{
    QByteArray salida,empty;
    bool yasecuantos=false;
    int bytesEsperados=3;
    if(numRespuesta!=0)
    {
        bytesEsperados=numRespuesta;
        yasecuantos=true;
    }
  // int bytesEsperados=numRespuesta;
   bool salir=false;
   QDateTime ahora=QDateTime::currentDateTime();
   QDateTime antes=ahora;
   QByteArray ms;
    while(!salir)
   {
        QApplication::processEvents();
       ahora=QDateTime::currentDateTime();
       if(antes.msecsTo(ahora)>tiempo)
       {
           salir=true;
           //ui->labelUltimoRecibido->setText("sin respuesta");
           //this->bufferAppCircuito.clear();
           return ms;
       }
       while( serial_port.rdbuf()->in_avail() > 0  )
       {
           QApplication::processEvents();
            //this->resetFallosConsecutivos();//=0;
           // qDebug()<<"in";
            char next_byte;//=new char[1];
            char f[1];
            serial_port.get(next_byte);
            f[0]=next_byte;
            salida.append(QByteArray(f)[0]);
            if(numRespuesta==0)
            {
                if(salida.count()>3)
                {
                    bytesEsperados=salida[2]+5;
                    yasecuantos=true;
                }
            }

            if(yasecuantos)
            {
                if(salida.count()==bytesEsperados)
                {
                    ms=salida;//this->bufferAppCircuito;
                   // ms.truncate(bytesEsperados);
                    this->bufferAppCircuito.clear();
                    salir=true;
                }
                else
                {
                   // qDebug()<<"error en la lectura....";
                }

            }
           // qDebug()<<"lo leido hasta ahora:"<<ms.toHex();

       }
   }
   if(salir)
   {
           salida=ms;
   }
  // this->bufferAppCircuito.clear();
   return salida;
}


/////////////////////////////////////77
void Estacion::agregarOrdenParaBase(QByteArray msg)
{
    int bas=-1;
    int idbase=msg[0];
    for(int i=0;i<this->mBases.count();i++)
    {
        if(this->mBases[i]->getId()==idbase)
        {
            bas=i;
            break;
        }
    }
    if(!this->mBases[bas]->getBloqueada())
    {
        this->listaOrdenes.append(msg);
    }
}

void Estacion::agregarMensajeAListaParaCliente(QByteArray msg,bool engancheOdesenganche)
{
    if(engancheOdesenganche)
    {
        QSqlQuery q;
        QString sentencia="insert into pendientes (idestacion,mensaje,tipo) values ("+QString::number(this->idEstacion)+",\""+msg.toHex()+"\",4)";
        if(q.exec(sentencia))
        {
            this->listaParaClienteEnganchesYdesenganches.append(msg);

        }
        else
        {
            qDebug()<<"Error en la sentencia sql:"<<sentencia<<"->"<<q.lastError().text();
        }
    }
    else
    {
        this->listaParaClienteEstados.append(msg);
       // qDebug()<<"Agrego para los estados:"<<msg.toHex()<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
    }
}
void Estacion::procesarRespuesta(QByteArray msg)
{
    int cmd=msg[1];
    switch(cmd)
    {
        case COMANDO_OBTENER_DIRECCION_BASE        :// 0x80;
        {
            break;
        }
        case COMANDO_ESCRIBIR_DIRECCION_BASE       :// 0x81;
        {
            break;
        }
        case COMANDO_ESCRIBIR_INSTALACION_NUM_TRJ  :// 0x82;
        {
            break;
        }
        case COMANDO_LEER_INSTALACION_NUM_TRJ      :// 0x83;
        {
            break;
        }

        case COMANDO_PUESTA_EN_HORA                :// 0x85;
        {
            break;
        }
        case COMANDO_OBTENER_HORA                  :// 0x86;
        {
            break;
        }


        case COMANDO_PARAMETROS_DE_FABRICA     :// 0x00;
        {
            break;
        }
        case COMANDO_PROGRAMAR_PARAMETROS      :// 0x01;
        {
            break;
        }
        case COMANDO_LEER_PARAMETROS           :// 0x02;
        {
            break;
        }
        case COMANDO_PROGRAMAR_SUPERUSUARIOS   :// 0x03;
        {
            break;
        }
        {
            break;
        }
        case COMANDO_INTERROGAR_SUPERUSUARIOS  :// 0x04;
        {
            break;
        }
        case COMANDO_INTERROGAR_RFID_DEVICE    :// 0x05; //No lo utilizamos
        {
            break;
        }

        ///// ORDENES ////////////////////////////////////////

        case COMANDO_ENVIAR_ORDEN_A_LA_BASE      :// 0x10;
        {
            break;
        }


        ///////////////////////////////////////////////////////

        case COMANDO_LIBERAR_CON_PARAMETROS        :// 0x11;
        {
            break;
        }
        case COMANDO_BORRAR_DATOS_BICI             :// 0x12;
        {
            break;
        }
        case COMANDO_OBTENER_ESTADO                :// 0x20;
        {
            this->procesarEstado(msg);
            break;
        }
        case COMANDO_OBTENER_DATOS_FIN_RECORRIDO   :// 0x21;
        {
            this->procesarDatosFinDeRecorrido(msg);
            break;
        }
        case COMANDO_PETICION_LECTURA_SECTOR_BICI  :// 0x22;
        {
            break;
        }
        case COMANDO_LEER_DATOS_SECTOR_BICI        :// 0x25;
        {
            break;
        }

        case COMANDO_ENVIAR_SALIDAS                :// 0x30;
        {
            break;
        }
    }


}



STR_INPUTS Estacion::obtenerInputs(int neo)
{
    UNI_INPUTS uniTemp;
    QByteArray byteInputs=intToBits(neo);
    memcpy(uniTemp.datos,byteInputs,byteInputs.count());

    return uniTemp.estructura;
}
STR_ALARMAS Estacion::obtenerAlarmas(int neo)
{
    UNI_ALARMAS uniTemp;
    QByteArray byteAlarmas=intToBits(neo);
    memcpy(uniTemp.datos,byteAlarmas,byteAlarmas.count());

    return uniTemp.estructura;
}
void Estacion::sacarDeListaDeApestados(QByteArray idrfid)
{
    for(int i=0;i<this->listaApestados.count();i++)
    {
        if(this->listaApestados.at(i).idrfid==idrfid)
        {
            this->listaApestados.removeAt(i);
            break;
        }
    }
}

bool Estacion::estaApestado(QByteArray idrfid)
{
    bool esta=false;

    for(int i=0;i<this->listaApestados.count();i++)
    {
        if(this->listaApestados.at(i).idrfid==idrfid)
        {
            esta=true;
            break;
        }
    }

    return esta;
}
bool Estacion::tieneAlgunApestadoEstaBase(int bas,QByteArray* id)
{
    bool esta=false;

    for(int i=0;i<this->listaApestados.count();i++)
    {
        if(this->listaApestados.at(i).bas==bas)
        {
            *id=this->listaApestados.at(i).idrfid;
            esta=true;
            break;
        }
    }

    return esta;
}

void Estacion::agregarAListaDeApasetados(QByteArray idrfid,int bas)
{
    STR_APESTADO strApestado;
    bool estaba=false;
    for(int i=0;i<this->listaApestados.count();i++)
    {
        if(this->listaApestados.at(i).idrfid==idrfid)
        {
            estaba=true;
            break;
        }
    }

    if(!estaba)
    {
        strApestado.bas=bas;
        strApestado.idrfid=idrfid;
        this->listaApestados.append(strApestado);
    }
}

bool Estacion::comprobarRFID(QByteArray id,int estado,int bas)
{
    bool salida=false;

    if(!this->estaApestado(id))
    {
        QSqlQuery q;

       // QString sentencia="select saldo,tipo,duracion,fecha from vav_abono where idrfid='"+id.toHex()+"' and activado=1";
        QString idrfid=id.toHex();

        QString sentencia="select idrfid, saldo, tipo, duracion, fecha, orden from  (select idrfid, saldo, tipo, duracion, fecha, orden from (              select idrfid, saldo,tipo,duracion,fecha, \"1\" as orden from vav_abono where idrfid='"+idrfid+"' and activado=1 union select idrfid, saldo,tipo,duracion,fecha, \"2\" as orden from abono where idrfid='"+idrfid+"' and activado=1          ) as abono_u          order by fecha desc) abono_u2          order by orden asc limit 1";




        qDebug()<<"sentencia:"<<sentencia;


        bool done=false;
        int cause=0;

        QDateTime antes=QDateTime::currentDateTime();
        int intento=0;
        while(!done)
        {
           // sleep(1);
            QApplication::processEvents();
            //sentencia="select * from vav_abono";
            if(q.exec(sentencia))
            {
                cause=0;
                done=true;

                if(q.next())
                {
                    int tipo=q.value(2).toInt();
                    if(tipo==TIPO_RESIDENTE)
                    {   if(estado==ESTADO_BICI_ENGANCHADA)
                        {
                            double saldo=q.value(1).toDouble();
                            if(saldo>0.5)
                            {
                                salida=true;
                                this->agregarAListaDeApasetados(id,bas);
                            }
                        }
                    }
                    else if(tipo==TIPO_TRABAJADOR)
                    {
                        salida=true;
                    }
                    else if(tipo==TIPO_TURISTA)
                    {
                        double saldo=q.value(1).toDouble();
                        double duracion=q.value(3).toInt();
                        QDateTime fechaIni=q.value(4).toDateTime();
                        QDateTime fechaFin=fechaIni.addDays(duracion);

                        QDateTime ahora=QDateTime::currentDateTime();
                        if(ahora>fechaFin)
                        {
                            if(saldo>0.5)
                            {
                                salida=true;
                                this->agregarAListaDeApasetados(id,bas);
                            }
                        }
                    }
                }




                break;
            }
            else if(q.lastError().text()!="Table 'neo_bonopark_madrid.vav_abono' doesn't exist QMYSQL: Unable to execute query")
            {
                done=true;
                cause=2;
                break;

            }
            else
            {
                qDebug()<<intento++;
                if(intento>5)
                {
                    done=true;
                }
            }
            sleep(1);

        }
        if(cause==0)
        {
            qDebug()<<"good!";

        }
        else
        {
            qDebug()<<"La causa es distinta.";
        }



    }
    return salida;
}
bool Estacion::resolver(QSqlQuery q,int estado,int bas,QByteArray id)
{
    bool salida=false;
    if(q.next())
    {
        int tipo=q.value(1).toInt();
        if(tipo==TIPO_RESIDENTE)
        {   if(estado==ESTADO_BICI_ENGANCHADA)
            {
                double saldo=q.value(0).toDouble();
                if(saldo>0.5)
                {
                    salida=true;
                    this->agregarAListaDeApasetados(id,bas);
                }
            }
        }
        else if(tipo==TIPO_TRABAJADOR)
        {
            salida=true;
        }
        else if(tipo==TIPO_TURISTA)
        {
            double saldo=q.value(0).toDouble();
            double duracion=q.value(2).toInt();
            QDateTime fechaIni=q.value(3).toDateTime();
            QDateTime fechaFin=fechaIni.addDays(duracion);

            QDateTime ahora=QDateTime::currentDateTime();
            if(ahora>fechaFin)
            {
                if(saldo>0.5)
                {
                    salida=true;
                    this->agregarAListaDeApasetados(id,bas);
                }
            }
        }
    }
    return salida;
}

void Estacion::procesarDatosFinDeRecorrido(QByteArray msg)
{
    int idbase=msg[0];
    int numDatos=msg[2];
    int bas=-1;
    for(int i=0;i<this->mBases.count();i++)
    {
        if(this->mBases[i]->getId()==idbase)
        {
            bas=i;
            break;
        }
    }
    if(bas!=-1)
    {
        UNI_DATOS_FIN_RECORRIDO uniDatosFinRecorrido;
        QByteArray datos=msg.mid(3,numDatos);
        memcpy(uniDatosFinRecorrido.datos,datos,datos.count());
        this->mBases[bas]->setUniDatosFinRecorrido(uniDatosFinRecorrido);
        this->mBases[bas]->setEsperandoDatosFinDeRecorrido(false);
        this->mBases[bas]->setAlmacenadosDatosDeRecorrido(true);
    }

}
void Estacion::cobrarPorUso(QByteArray idrfid,double minutos,quint16 *saldoInicial,quint16*saldoFinal,bool reservado)
{
    int tipo=-1;
double total;//=50.0;
    /*turista

      2€ primera hora, 4€ las demas.

      */
    QSqlQuery q;
    QString sentencia="select tipo from vav_abono where activado=1 and idrfid='"+idrfid.toHex()+"'";

    if(q.exec(sentencia))
    {
        if(q.next())
        {
           tipo=q.value(0).toInt();


        }
    }





   /* Primera fracción de 30 minutos 0.50€

    Siguientes fracciones de treinta minutos 0.60€

    Bonificación por coger la bicicleta en estación excedentaria 0.10€

    Bonificación por devolver la bicicleta en estación definitiva 0.10€

    Tarifa de penalización por haber excedido las dos horas, por hora o fracción 4€*/




    if(tipo==TIPO_RESIDENTE)
    {
        if(minutos<30)
        {
            total=0.5;
        }
        else if(minutos<120)
        {
            double parcial=0.5;
            double min_posterior=minutos-29;
            total=parcial+((int)min_posterior/(int)30+1)*0.6;
        }
        else if(minutos>=120)
        {
            /*30->0.5
              30-120->x/30*0.6
              >120-> 2,4+0,5+4X
              */
            double parcial=0.5+((int)120/(int)30+1)*0.6;
            double min_posterior=minutos-119;
            total=parcial+((int)min_posterior/(int)60+1)*4;

        }
        else
        {
            qDebug()<<"?????????";
        }
    }
    else if(tipo==TIPO_TURISTA)
    {
        total=2+minutos/60;
    }

    if(reservado)
    {
        total-=0.1;
    }
    qDebug()<<"total:"<<total;


   sentencia="select saldo from vav_abono where activado=1 and idrfid='"+idrfid.toHex()+"'";
   double nuevoSaldo=0;
    if(q.exec(sentencia))
    {
        if(q.next())
        {
            *saldoInicial=q.value(0).toDouble()*100;
            *saldoFinal=*saldoInicial-100*total;
            nuevoSaldo=(double)*saldoFinal/(double)100;

        }
    }
    QString sentencia1="update vav_abono set saldo = "+QString::number((double)*saldoFinal/double(100))+" where idrfid='"+idrfid.toHex()+"'";
    if(q.exec(sentencia1))
    {
        if(q.numRowsAffected()>0)
        {
            qDebug()<<"Actualizado correctamente!";
        }
        else
        {
            qDebug()<<"No afecta a ningun usuario la actualizacion del saldo";
        }
    }
    else
    {
        qDebug()<<"Error:"<<q.lastError().text();
        qDebug()<<"sentencia:"<<sentencia1;
    }


}
quint8 Estacion::getEstadoSemaforo(bool desenganchando)
{
    quint8 luz=0;

    int bicisEnganchadas=0;
    int basesTotales=this->mBases.count();

    QSqlQuery q;

    QString sentencia="select count(*) from base where idestacion="+QString::number(this->idEstacion)+" and estado = 3";//los enganchadas

    if(q.exec(sentencia))
    {
        if(q.next())
        {
            bicisEnganchadas=q.value(0).toInt();
        }
    }
    if(!desenganchando)
    {
        bicisEnganchadas--;
    }
    if(bicisEnganchadas*100/basesTotales>75)
    {
        luz=1;
    }
    return luz;
}

void Estacion::procesarDesenganche(int bas)
{
    //ES UN DESENGANCHE!!//
    STR_DESENGANCHE dsg=this->mBases[bas]->getDatosDesenganchePendiente();

    UNI_MENSAJE_DESENGANCHE uniDes;
   // QString sentencia;
    quint16 saldo=0;

  //  quint16 saldo_nuevo=0;
    QSqlQuery q;
    QString sentencia;
    sentencia="select saldo from vav_abono where idrfid='"+dsg.idrfid.toHex()+"'";
    if(q.exec(sentencia))
    {
        if(q.next())
        {
            double sal=q.value(0).toDouble();
           // saldo_nuevo=(sal-0.5)*100;
            saldo=sal*100;
        }
    }

   // quint16 saldo_nuevo=0;

   // this->actualizarBicisSimultaneas(dsg.idrfid,DESENGANCHE);

    memcpy(uniDes.estructura.idbici,dsg.idbici,dsg.idbici.count());
    memcpy(uniDes.estructura.idrfid,dsg.idrfid,dsg.idrfid.count());

    QByteArray bytesaldonuevo;//(reinterpret_cast<char*>(&saldo_nuevo), 2);
    bytesaldonuevo.append(H(saldo));
    bytesaldonuevo.append(L(saldo));
    for(int u=0;u<2;u++)uniDes.estructura.saldo_final[u]=bytesaldonuevo[u];


    QByteArray bytesaldoantiguo;//(reinterpret_cast<char*>(&saldo), 2);
    bytesaldoantiguo.append(H(saldo));
    bytesaldoantiguo.append(L(saldo));
    for(int u=0;u<2;u++)uniDes.estructura.saldo_inicial[u]=bytesaldoantiguo[u];

    uniDes.estructura.luz=luz;
    QByteArray msg(reinterpret_cast<char*>(&uniDes.valores), sizeof(uniDes.estructura));

    QByteArray envio=this->empaquetarParaCliente(msg,COMANDO_SRV_ENVIO_DESENGANCHE_OK,bas);
    this->agregarMensajeAListaParaCliente(envio,true);
}
void Estacion::reservaPisada(int bas)
{
    QString idrfid;//=q.value(0);
    QDateTime fecha_fin;//=q.value(1).toDateTime();
    bool ok=false;
    QSqlQuery q;
    QString sentencia="select idrfid,fecha_fin from reserva where idestacion="+QString::number(this->idEstacion)+" and idbase="+QString::number(this->mBases[bas]->getId());
    if(q.exec(sentencia))
    {
        if(q.next())
        {
            idrfid=q.value(0).toString();
            fecha_fin=q.value(1).toDateTime();
            ok=true;
        }
    }
    sentencia="select idbase from base where idestacion="+QString::number(this->idEstacion)+ " and estado=4";
    if(q.exec(sentencia))
    {
        if(q.next())
        {
            //QString sentencio="update reserva"
        }
    }
}

bool Estacion::comprobarEngancheEnReserva(QByteArray r, int bas)
{
    bool salida=false;
    int idbase;
    QSqlQuery q;
    QString sentencia="select idbase,fecha_reserva,fecha_fin from reservas where idrfid='"+r.toHex()+"' and estado=1";
    if(q.exec(sentencia))
    {
        if(q.next())
        {
            idbase=q.value(0).toInt();
            QDateTime fecha_fin=q.value(2).toDateTime();
            QDateTime ahora=QDateTime::currentDateTime();
            if(ahora>fecha_fin)
            {
                salida=false;
            }
            else
            {
                salida=true;
            }
        }
        else
        {
            qDebug()<<"Alguien ha enganchado una bici en una base reservada sin haber realizado reserva....";
            this->reservaPisada(bas);
            //pendiente//
        }
        if(salida)
        {
            if(idbase==this->mBases[bas]->getId())
            {
                sentencia="update reservas set estado=2 where estado=1 and idrfid='"+r.toHex()+"'";
                if(q.exec(sentencia))
                {
                    if(q.numRowsAffected()>0)
                    {
                        qDebug()<<"Reserva finalizada correctamente por "<<r.toHex();
                        this->agregarOrdenParaBase(this->mBases[bas]->enviarOrden(ORDEN_FIN_BASE_RESERVADA));

                    }
                    else
                    {
                        qDebug()<<"No se ha actualizado nada....";
                    }
                }
                else
                {
                    qDebug()<<"Error en la sentencia:"<<sentencia;
                }
            }
            else
            {

                QSqlQuery q1;
                QString sentencia1="update reservas set idbase="+QString::number(this->mBases[bas]->getId())+",estado=2 where idrfid='"+r.toHex()+"' and estado=1";
                if(q1.exec(sentencia1))
                {
                    if(q1.numRowsAffected()>0)
                    {
                        QSqlQuery q2;
                        QString sentencia2="update reservas set idbase="+QString::number(idbase)+" where idbase="+QString::number(this->mBases[bas]->getId())+" and estado=1";
                        if(q2.exec(sentencia2))
                        {
                            if(q2.numRowsAffected()>0)
                            {
                                qDebug()<<"Actualizado correctamente el intercambio";
                                db.commit();
                            }
                            else
                            {
                                qDebug()<<"No ha afectado a ningun registro:"<<sentencia2;
                                db.rollback();
                            }
                        }
                        else
                        {
                            qDebug()<<"Error en sentencia sql:"<<sentencia2;
                            db.rollback();
                        }

                    }
                    else
                    {
                        qDebug()<<"No ha afectado a ningun registro..."<<sentencia1;
                        db.rollback();
                    }
                }
                else
                {
                    qDebug()<<"Error en sentencia sql..."<<sentencia1;
                    db.rollback();
                }


            }
        }

    }
    else
    {
        qDebug()<<"Error en la sentencia sql:"<<sentencia;
        qDebug()<<q.lastError().text();
    }
    return salida;
}

void Estacion::actualizarBicisSimultaneas(QByteArray idrfid, int cuando)
{
    bool ok;
    int bicis=255;
    QSqlQuery q1;
    QString sentencia_bicis="select bicis_simultaneas from vav_abono where idrfid='"+idrfid.toHex()+"'";
    if(q1.exec(sentencia_bicis))
    {
        if(q1.next())
        {
            bicis=q1.value(0).toInt();
            ok=true;
        }
        else
        {
            qDebug()<<"No hay abono con esta rfid";
        }
    }
    else
    {
        qDebug()<<"Problema con sentencia sql:"<<sentencia_bicis;
    }
    if(ok)
    {
        if(cuando==ENGANCHE)
        {
            sentencia_bicis="update abono set bicis_simultaneas="+QString::number(bicis-1)+" where idrfid='"+idrfid.toHex()+"'";
        }
        else if(cuando==DESENGANCHE)
        {
            sentencia_bicis="update abono set bicis_simultaneas="+QString::number(bicis+1)+" where idrfid='"+idrfid.toHex()+"'";
        }
        if(q1.exec(sentencia_bicis))
        {
            qDebug()<<"Actualizadas las bicis simultaneas del abono";
        }
        else
        {
            qDebug()<<"Error en la sentencia sql:"<<sentencia_bicis<<":"<<q1.lastError().text();
        }
    }
}
void Estacion::comprobarSemaforico()
{


    qDebug()<<"comprobando semaforo...";
    int total=0;
    int bases_ocupada=0;
    int bases_libres=0;

    QSqlQuery q;
    QString sentencia="select estado from base where idestacion="+QString::number(this->idEstacion);
    if(q.exec(sentencia))
    {
        while(q.next())
        {
            int estado=q.value(0).toInt();
            if(estado==ESTADO_BICI_ENGANCHADA)
            {
                bases_ocupada++;
                total++;
            }
            if(estado==ESTADO_BASE_LIBRE)
            {
                bases_libres++;
                total++;
            }
        }
    }
    int numerador=bases_ocupada*100/total;
    if(numerador>75)
    {
        this->encederRojo();
    }
    else
    {
        encenderVerde();
    }
    qDebug()<<"terminado de comprobar semaforo!";
}

void Estacion::procesarEstado(QByteArray msg)
{
    int idbase=msg[0];
    int numDatos=msg[2];
    int bas=-1;
    for(int i=0;i<this->mBases.count();i++)
    {
        if(this->mBases[i]->getId()==idbase)
        {
            bas=i;
            break;
        }
    }
    if(bas!=-1)
    {
        UNI_ESTADO_BASE uniEstadoAnterior=this->mBases[bas]->getUniEstado();
        UNI_ESTADO_BASE uniEstadoActual;
        QByteArray datos=msg.mid(3,numDatos);
        memcpy(uniEstadoActual.datos,datos,datos.count());
        this->mBases[bas]->setUniEstado(uniEstadoActual);

        STR_INPUTS inputsActual=this->obtenerInputs(uniEstadoActual.estructura.inputs);
        STR_ALARMAS alarmasActual=this->obtenerAlarmas(uniEstadoActual.estructura.alarmas);

        QSqlQuery q;
        QString sentencia="update base set estado="+QString::number(uniEstadoActual.estructura.estado);
        sentencia+=", fecha_cambio_estado='"+QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss")+"'";
        sentencia+=", input_sensor="+QString::number(inputsActual.sensor);
        sentencia+=", input_interruptor="+QString::number(inputsActual.interruptor);
        sentencia+=", input_aux1="+QString::number(inputsActual.aux1);
        sentencia+=", input_aux2="+QString::number(inputsActual.aux2);
        sentencia+=", input_calibracion="+QString::number(inputsActual.cal);
        sentencia+=", input_programacion="+QString::number(inputsActual.prog);

        QByteArray byteidbici(reinterpret_cast<char*>(&uniEstadoActual.estructura.idbici), 4);
        QByteArray byteidbiciAntiguo(reinterpret_cast<char*>(&uniEstadoAnterior.estructura.idbici), 4);


        sentencia+=", idbici='"+byteidbici.toHex()+"'";

        QByteArray byteidrfid(reinterpret_cast<char*>(&uniEstadoActual.estructura.rfid), 7);

        sentencia+=", idrfid='"+byteidrfid.toHex()+"'";
        sentencia+=", error="+QString::number(uniEstadoActual.estructura.errores);
        sentencia+=", nivel="+QString::number(uniEstadoActual.estructura.nivel);
        int tensionBateria=256*uniEstadoActual.estructura.vbat[0]+uniEstadoActual.estructura.vbat[1];
        sentencia+=", vbat="+QString::number(tensionBateria);
        sentencia+=", icarga="+QString::number(uniEstadoActual.estructura.icarga);
        sentencia+=", error_microcontrolador_auxiliar="+QString::number(alarmasActual.error_microcontrolador_auxiliar);
        sentencia+=", pwm="+QString::number(uniEstadoActual.estructura.pwm)+" where idbase="+QString::number(idbase)+" and idestacion ="+QString::number(this->idEstacion);
        if(!q.exec(sentencia))
        {
            qDebug()<<sentencia;
            qDebug()<<q.lastError().text();
            qDebug()<<"ups...";
        }


        int estadoAnterior=uniEstadoAnterior.estructura.estado;
        int estadoActual=uniEstadoActual.estructura.estado;
        int error=uniEstadoActual.estructura.errores;
        bool transicion=false;
        bool cambio=false;
        if( uniEstadoAnterior.estructura.estado!=uniEstadoActual.estructura.estado)
        {
            transicion=true;
            cambio=true;
            this->comprobarSemaforico();
        }
        if( (uniEstadoAnterior.estructura.inputs!=uniEstadoActual.estructura.inputs)||
                (byteidbici.toHex()!=byteidbiciAntiguo.toHex()))
        {
            cambio=true;
        }
        if(cambio)
        {
            QByteArray msg=this->enviarEstadoAServidor(bas);
            this->agregarMensajeAListaParaCliente(msg,false);

        }




        if(transicion)
        {


            if((estadoActual==ESTADO_BICI_ENGANCHADA))
            {
                QByteArray rfidApestado;
                if(this->tieneAlgunApestadoEstaBase(bas,&rfidApestado))
                {
                    this->sacarDeListaDeApestados(rfidApestado);
                }
                if((estadoAnterior==ESTADO_MIRA_BATERIA)||
                 (estadoAnterior==ESTADO_BASE_LIBRE)||
                 ((estadoAnterior==ESTADO_ERROR)&&(error==ERROR_ENGANCHE)))
                {
                    this->mBases[bas]->setEsperandoDatosFinDeRecorrido(true);

                    qDebug()<<"pidiendo datos de fin de recorrido...";
                }
                if(estadoAnterior==ESTADO_BASE_RESERVADA)
                {
                    this->mBases[bas]->setEsperandoDatosFinDeRecorrido(true);
                    this->mBases[bas]->setEstabaReservada(true);
                }
            }
            else if(estadoActual==ESTADO_BASE_LIBRE)
            {
                QByteArray rfidApestado;
                if(this->tieneAlgunApestadoEstaBase(bas,&rfidApestado))
                {
                    this->sacarDeListaDeApestados(rfidApestado);
                }
                if(estadoAnterior==ESTADO_BICI_ENGANCHADA)
                {
                    this->procesarDesenganche(bas);


                }

            }
            else if(estadoActual==ESTADO_ERROR)
            {
                if(error==ERROR_DESENGANCHE)
                {
                    STR_DESENGANCHE strDesenganche=this->mBases[bas]->getDatosDesenganchePendiente();
                    QByteArray rfidApestado;
                    if(this->tieneAlgunApestadoEstaBase(bas,&rfidApestado))
                    {
                        this->sacarDeListaDeApestados(rfidApestado);
                    }
                    if( (inputsActual.interruptor==1)&&
                            (inputsActual.aux1==1)&&
                            (!alarmasActual.error_bici==1)&&
                            (alarmasActual.hay_bateria==1))
                    {
                        qDebug()<<"SE HA QUEDADO ENGANCHADA... HAY QUE MANDAR UN DESENGANCHE KO";
                        UNI_DESENGANCHE_KO uniDesengancheKo;
                        for(int u=0;u<4;u++)uniDesengancheKo.estructura.idbici[u]=strDesenganche.idbici[u];
                        for(int u=0;u<7;u++)uniDesengancheKo.estructura.idrfid[u]=strDesenganche.idrfid[u];
                        qDebug()<<strDesenganche.idrfid.toHex();
                        char* p_Char;
                        p_Char=reinterpret_cast<char*>(&uniDesengancheKo.estructura);

                        QByteArray msgDesengancheKO(p_Char,11);
                        qDebug()<<"msgdes:"<<msgDesengancheKO.toHex();
                        p_Char=0;
                        delete p_Char;

                        QByteArray paq=this->empaquetarParaCliente(msgDesengancheKO,COMANDO_SRV_ENVIO_DESENGANCHE_KO,bas);
                        qDebug()<<"El mensaje de desenganche ko va a ser:"<<paq.toHex();

                        this->agregarMensajeAListaParaCliente(paq,true);

                    }
                    else
                    {
                        if((inputsActual.interruptor==0)||
                                (inputsActual.aux1==0))
                        {
                            qDebug()<<"Pero ha dejado la bici aqui!!";
                            this->procesarDesenganche(bas);
                        }
                    }
                }
            }
            else if(estadoActual==ESTADO_BASE_RESERVADA)
            {

                for(int u=0;u<this->mStrReservas.count();u++)
                {
                    if(mStrReservas[u].idbase==bas)
                    {
                        qDebug()<<"Lo emito a las:"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                        emit this->sgnReservaBase();

                    }
                    this->mStrReservas.removeAt(u);
                }
            }

        }
        QByteArray rfidAnterior;
        for(int i=0;i<7;i++)
        {
            rfidAnterior.append(uniEstadoAnterior.estructura.rfid[i]);
        }
        if(bas==0)
        {
           // qDebug()<<rfidAnterior.toHex();
        }
        if(rfidAnterior.toHex()=="00000000000000")
        {
            QByteArray rfidActual;
            for(int i=0;i<7;i++)
            {
                rfidActual.append(uniEstadoActual.estructura.rfid[i]);
            }
            if(rfidActual.toHex()!="00000000000000")
            {
                if(this->comprobarRFID(rfidActual,estadoActual,bas))
                {

                    STR_DESENGANCHE dTemp;
                    dTemp.fecha_permiso=QDateTime::currentDateTime();

                    QByteArray idbici(reinterpret_cast<char*>(&uniEstadoActual.estructura.idbici), 4);
                    dTemp.idbici=idbici;

                    QByteArray idrfid(reinterpret_cast<char*>(&uniEstadoActual.estructura.rfid), 7);
                    dTemp.idrfid=idrfid;

                    dTemp.semaforo=this->getEstadoSemaforo(true);
                    this->mBases[bas]->setDatosDesenganchePendiente(dTemp);


                    if(estadoActual==ESTADO_BICI_ENGANCHADA)
                    {
                        this->agregarOrdenParaBase(this->mBases[bas]->enviarOrdenLiberarConParametros(this->idEstacion,rfidActual));
                    }
                    else
                    {
                        this->agregarOrdenParaBase(this->mBases[bas]->enviarOrden(ORDEN_LIBERAR_SIN_PARAMETROS));
                    }
                }
                else
                {
                    this->agregarOrdenParaBase(this->mBases[bas]->enviarOrden(ORDEN_TARJETA_INCORRECTA));
                }
            }
        }

        switch(estadoActual)
        {
            case ESTADO_RESET :// 0;
            case ESTADO_INICIO :// 1;
            case ESTADO_MIRA_BATERIA :// 2;
            case ESTADO_BICI_ENGANCHADA :// 3;
            {
                if(this->mBases[bas]->getEsperandoDatosFinDeRecorrido())
                {

                    STR_ALARMAS alarmasActual=this->obtenerAlarmas(uniEstadoActual.estructura.alarmas);
                    if(alarmasActual.leido_fin_recorrido==1)
                    {
                        this->mBases[bas]->setAlmacenadosDatosDeRecorrido(false);
                        this->agregarOrdenParaBase(this->mBases[bas]->peticionDatosFinRecorrido());
                        this->mBases[bas]->setEsperandoDatosFinDeRecorrido(false);
                    }
                    else
                    {
                        QDateTime ahora=QDateTime::currentDateTime();
                        int dif=this->mBases[bas]->getFechaInicioPeticionDatosRecorrido().msecsTo(ahora);
                        if(dif>1000)
                        {

                        }
                    }
                }
                else if(this->mBases[bas]->getAlmacenadosDatosDeRecorrido())
                {
                    this->mBases[bas]->setAlmacenadosDatosDeRecorrido(false);
                    UNI_DATOS_FIN_RECORRIDO datosFinRecorrido=this->mBases[bas]->getUniDatosFinRecorrido();
                    int idbaseDesenganche= datosFinRecorrido.estructura.base;
                    int semaforicoDesenganche=0;
                    if(idbaseDesenganche>128)
                    {
                        idbaseDesenganche-=128;
                        semaforicoDesenganche=1;
                    }

                    int bcdday=datosFinRecorrido.estructura.day;
                    int day=bcd2dato(bcdday);

                    int bcdmonth=datosFinRecorrido.estructura.month;
                    int month=bcd2dato(bcdmonth);

                    int bcdyear=datosFinRecorrido.estructura.year;
                    int year=bcd2dato(bcdyear)+2000;

                    int bcdhour=datosFinRecorrido.estructura.hour;
                    int hour=bcd2dato(bcdhour);

                    int bcdmin=datosFinRecorrido.estructura.min;
                    int min=bcd2dato(bcdmin);

                    int bcdsec=datosFinRecorrido.estructura.sec;
                    int sec=bcd2dato(bcdsec);

                    QDateTime fechaDesenganche;
                    QDate dia(year,month,day);
                    QTime hora(hour,min,sec);
                    fechaDesenganche.setDate(dia);
                    fechaDesenganche.setTime(hora);

                    QByteArray idrfid;
                    for(int i=0;i<7;i++)idrfid.append(datosFinRecorrido.estructura.idrfid[i]);
                    QByteArray idbici;
                    for(int i=0;i<4;i++)idbici.append(datosFinRecorrido.estructura.idbici[i]);

                    bool conReserva=false;
                    if(this->mBases[bas]->getEstabaReservada())
                    {
                        conReserva=true;
                        this->comprobarEngancheEnReserva(idrfid,bas);
                        this->mBases[bas]->setEstabaReservada(false);

                    }

                    QDateTime ahora=QDateTime::currentDateTime();

                    int segundosDeDiferencia=fechaDesenganche.time().secsTo(ahora.time());
                    int minutosDeDiferencia=segundosDeDiferencia/60;
                    quint16 saldoFinal=0;
                    quint16 saldoInicial=0;
                    this->cobrarPorUso(idrfid,minutosDeDiferencia,&saldoInicial,&saldoFinal,conReserva);

/*
                    double dSaldoInicial=saldoInicial/(double)100.0;
                    double dSaldoFinal=saldoFinal/(double)100.0;



                    int idEstacionDesenganche=256*datosFinRecorrido.estructura.estacion[0]+datosFinRecorrido.estructura.estacion[1];

                    QSqlQuery q;
                    QString sentencia;

                 //   this->actualizarBicisSimultaneas(idrfid,ENGANCHE);


                    sentencia="insert into enganche (idestacion_desenganche,idbase_desenganche,fecha_desenganche,idestacion_enganche,idbase_enganche,fecha_enganche,idrfid,idbici,saldo_anterior,saldo_nuevo)";
                    QString strIdrfid=idrfid.toHex();
                    QString strIdBici=idbici.toHex();
                    sentencia+=QString(" values (%1,%2,'%3',%4,%5,'%6','%7','%8',%9,%10)")
                            .arg(idEstacionDesenganche)
                            .arg(idbaseDesenganche)
                            .arg(fechaDesenganche.toString("yyyy/MM/dd HH:mm:ss"))
                            .arg(this->idEstacion)
                            .arg(this->mBases[bas]->getId())
                            .arg(ahora.toString("yyyy/MM/dd HH:mm:ss"))
                            .arg(strIdrfid)
                            .arg(strIdBici)
                            .arg(dSaldoInicial)
                            .arg(dSaldoFinal);
                    if(q.exec(sentencia))
                    {
                       // bool ok=false;
                        qDebug()<<"enganche insertado correctamente.";
                       // QSqlQuery q1;
                       // int bicis=0;


                        QString sentencia_bicis="select bicis_simultaneas from abono where idrfid='"+idrfid.toHex()+"'";
                        if(q1.exec(sentencia_bicis))
                        {
                            if(q1.next())
                            {
                                bicis=q1.value(0).toInt();
                                ok=true;
                            }
                            else
                            {
                                qDebug()<<"No hay abono con esta rfid";
                            }
                        }
                        else
                        {
                            qDebug()<<"Problema con sentencia sql:"<<sentencia_bicis;
                        }
                        if(ok)
                        {
                            sentencia_bicis="update abonos set bicis_simultaneas="+QString::number(bicis-1)+" where idrfid='"+idrfid.toHex()+"'";
                            if(q1.exec(sentencia_bicis))
                            {
                                qDebug()<<"Actualizadas las bicis simultaneas del abono";
                            }
                        }
*/

                        UNI_MENSAJE_ENGANCHE uMensajeEnganche;

                        for(int u=0;u<2;u++)uMensajeEnganche.estructura.estacion_origen[u]=datosFinRecorrido.estructura.estacion[u];


                        uMensajeEnganche.estructura.base_origen=datosFinRecorrido.estructura.base;
                        uMensajeEnganche.estructura.day_denganche=bcdday;
                        uMensajeEnganche.estructura.month_denganche=bcdmonth;
                        uMensajeEnganche.estructura.year_desenganche=bcdyear;

                        uMensajeEnganche.estructura.hour_desenganche=bcdhour;
                        uMensajeEnganche.estructura.minute_denganche=bcdmin;
                        uMensajeEnganche.estructura.second_denganche=bcdsec;

                        memcpy(uMensajeEnganche.estructura.idbici,idbici,idbici.count());
                        memcpy(uMensajeEnganche.estructura.idrfid,idrfid,idrfid.count());
                       // uMensajeEnganche.estructura.luz_=1;
                        quint16 saldoInicialcentimos=saldoInicial;
                        quint16 saldoFinalCentimos=saldoFinal;


                        QByteArray byte_saldo_inicial;//(reinterpret_cast<char*>(&saldoInicialcentimos), sizeof(saldoInicialcentimos));
                        byte_saldo_inicial.append(H(saldoInicialcentimos));
                        byte_saldo_inicial.append(L(saldoInicialcentimos));

                        memcpy(uMensajeEnganche.estructura.saldo_anterior,byte_saldo_inicial,byte_saldo_inicial.count());

                        QByteArray byte_saldo_final;//(reinterpret_cast<char*>(&saldoFinalCentimos), sizeof(saldoFinalCentimos));
                        byte_saldo_final.append(H(saldoFinalCentimos));
                        byte_saldo_final.append(L(saldoFinalCentimos));

                        memcpy(uMensajeEnganche.estructura.saldo_nuevo,byte_saldo_final,byte_saldo_final.count());

                        int semaforicoEnganche=this->getEstadoSemaforo(false);
                        uMensajeEnganche.estructura.luz_enganche=semaforicoEnganche;
                        uMensajeEnganche.estructura.luz_desenganche=semaforicoDesenganche;
                        uMensajeEnganche.estructura.con_reserva=(conReserva)?1:0;

                        //memcpy(uMensajeEnganche.estructura.saldo_nuevo,saldoFinalCentimos,sizeof(saldoFinalCentimos));


                        QByteArray foo(reinterpret_cast<char*>(&uMensajeEnganche.valores), sizeof(uMensajeEnganche.estructura));
                        QByteArray msg=this->empaquetarParaCliente(foo,COMANDO_SRV_ENVIO_ENGANCHE,bas);
                        this->agregarMensajeAListaParaCliente(msg,true);





                }

                break;
            }
            case ESTADO_BASE_LIBRE :// 4;
        {
            break;
        }
            case ESTADO_COLECTA :// 5;
            case ESTADO_BICI_RESERVADA :// 6;
            case ESTADO_BASE_RESERVADA :// 7;
        {
            QSqlQuery q;
            QString sentencia="select fecha_fin from reservas where estado=1";
            if(q.exec(sentencia))
            {
                if(q.next())
                {
                    QDateTime fechaFin=q.value(0).toDateTime();
                    QDateTime ahora=QDateTime::currentDateTime();

                    if(ahora>fechaFin)//10 minutos
                    {
                        this->agregarOrdenParaBase(this->mBases[bas]->enviarOrden(ORDEN_FIN_BASE_RESERVADA));
                        QSqlQuery q1;
                        QString sentencia1="update reservas set estado=3 where idbase="+QString::number(this->mBases[bas]->getId())+ " and estado=1";
                        if(q1.exec(sentencia1))
                        {
                            qDebug()<<"Actualizado correctamente el estado de la reserva";
                        }
                        else
                        {
                            qDebug()<<"Error en la sentencia:"<<sentencia1;
                        }
                    }
                }
            }
            else
            {
                qDebug()<<"Error en la sentencia:"<<sentencia;
            }
            break;
        }



            case ESTADO_TEST :// 8;
            case ESTADO_ERROR :// 9;
            {
                if(error==ERROR_DESENGANCHE)
                {
                     if((inputsActual.sensor==1)&&
                             (inputsActual.interruptor==1)&&
                             (inputsActual.aux1==1)&&
                             (alarmasActual.hay_bateria)&&
                             (alarmasActual.error_bici==0))
                    {
                        this->agregarOrdenParaBase(this->mBases[bas]->enviarOrden(ORDEN_PASAR_A_BICICLETA_ENGANCHADA));

                    }
                    else if((inputsActual.sensor==0)&&(inputsActual.interruptor==0)&&(inputsActual.aux1==0))
                    {
                        this->agregarOrdenParaBase(this->mBases[bas]->enviarOrden(ORDEN_RESET));
                    }
                }
                else if(error==ERROR_ENGANCHE)
                {
                    if((inputsActual.sensor==1)&&
                            (inputsActual.interruptor==1)&&
                            (inputsActual.aux1==1)&&
                            (alarmasActual.hay_bateria==1)&&
                            (alarmasActual.error_bici==0))
                    {
                        this->agregarOrdenParaBase(this->mBases[bas]->enviarOrden(ORDEN_PASAR_A_BICICLETA_ENGANCHADA));

                    }
                    else if((inputsActual.sensor==0)&&(inputsActual.interruptor==0)&&(inputsActual.aux1==0))
                    {
                        this->agregarOrdenParaBase(this->mBases[bas]->enviarOrden(ORDEN_RESET));
                    }
                }
                break;
            }
        }









    }

   // qDebug()<<"ei";
}



QByteArray Estacion::empaquetarParaCliente(QByteArray msg,int cmd,int bas)
{
    QByteArray salida;

    QByteArray datos;
    datos.append(msg);

    QDateTime ahora=QDateTime::currentDateTime();
    int year1=ahora.date().year();
    int pre_year=year1/100;
    int year=year1-pre_year*100;
    int byteyear=dato2bcd(year);

    int month=ahora.date().month();
    int bytemonth=dato2bcd(month);

    int day=ahora.date().day();
    int byteday=dato2bcd(day);

    int hour=ahora.time().hour();
    int bytehour=dato2bcd(hour);

    int min=ahora.time().minute();
    int bytemin=dato2bcd(min);

    int sec=ahora.time().second();
    int bytesec=dato2bcd(sec);
    datos.append(byteyear);
    datos.append(bytemonth);
    datos.append(byteday);
    datos.append(bytehour);
    datos.append(bytemin);
    datos.append(bytesec);

    salida.append(H(this->idEstacion));
    salida.append(L(this->idEstacion));
    salida.append(this->mBases[bas]->getId());
    salida.append(cmd);
    salida.append(datos.count());
    salida.append(datos);

    quint16 crc=CalcularCRC(salida,salida.count());

    salida.append(H(crc));
    salida.append(L(crc));


    return salida;
}

QByteArray Estacion::enviarEstadoAServidor(int bas)
{
    QByteArray salida;

    UNI_ESTADO_BASE uniEstado=this->mBases[bas]->getUniEstado();

    int cmd=COMANDO_SRV_ENVIO_ESTADO;
    UNI_MENSAJE_PARA_SRV_ESTADO MSG;
    MSG.estructura.activada=(this->mBases[bas]->getBloqueada()==false)?1:0;
    MSG.estructura.alarmas=uniEstado.estructura.alarmas;
    MSG.estructura.errores=uniEstado.estructura.errores;
    MSG.estructura.estado=uniEstado.estructura.estado;
    MSG.estructura.icarga=uniEstado.estructura.icarga;
    for(int i=0;i<4;i++)  MSG.estructura.idbici[i]=uniEstado.estructura.idbici[i];
    MSG.estructura.inputs=uniEstado.estructura.inputs;
    MSG.estructura.nivel=uniEstado.estructura.nivel;
    MSG.estructura.pwm=uniEstado.estructura.pwm;
    for(int i=0;i<7;i++)MSG.estructura.rfid[i]=uniEstado.estructura.rfid[i];
    MSG.estructura.tipo=uniEstado.estructura.tipo;
    MSG.estructura.ttrj=uniEstado.estructura.ttrj;
    for(int i=0;i<2;i++)MSG.estructura.vbat[i]=uniEstado.estructura.vbat[2];
    MSG.estructura.version_bici=uniEstado.estructura.version_bici;

    char* p;
    p=reinterpret_cast<char*>(&MSG.estructura);
    QByteArray pre_salida(p,sizeof(MSG.estructura));
    p=0;
    delete p;
    salida=this->empaquetarParaCliente(pre_salida,cmd, bas);

    return salida;
}

///////////////////////////////////////
Estacion::~Estacion()
{
    delete ui;
}

void Estacion::on_pushButton_clicked()
{
    QByteArray d;
    double minutos=ui->lineEdit->text().toDouble();
    quint16 a,b;
    this->cobrarPorUso(d,minutos,&a,&b);
}

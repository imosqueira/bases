#ifndef ESTADOS_H
#define ESTADOS_H

const int ESTADO_RESET = 0;
const int ESTADO_INICIO = 1;
const int ESTADO_MIRA_BATERIA = 2;
const int ESTADO_BICI_ENGANCHADA = 3;
const int ESTADO_BASE_LIBRE = 4;
const int ESTADO_COLECTA = 5;
const int ESTADO_BICI_RESERVADA = 6;
const int ESTADO_BASE_RESERVADA = 7;
const int ESTADO_TEST = 8;
const int ESTADO_ERROR = 9;
const int ESTADO_NO_COMUNICA=10;

#endif // ESTADOS_H

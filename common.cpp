#include"common.h"
QByteArray intToBits(quint8 entero)
{
  //  entero=11;
   QByteArray salida;
   int ini=entero;
   while(ini>0)
   {
       int ret=ini%2;
       salida.prepend(quint8(ret));
       ini=ini/2;
   }
   for(int i=salida.count();i<8;i++)
   {
       quint8 cero=0x00;
       salida.prepend(cero);
   }


   return salida;
}
quint8 bitsToint(QByteArray bits, int hasi,int zenbat)
{
    quint8 salida=0;
    for(int i=0;i<zenbat;i++)
    {
        salida+=qPow(2,zenbat-i-1)*bits[hasi+i];
    }
    return salida;
}

quint8 dato2bcd(quint8 dato)
{
    quint8 salida;

    salida=(dato/10)*0x10+(dato%10);

    return salida;
}
quint8 bcd2dato(quint8 bcd)
{
    quint8 salida;

    salida=(bcd>>4)*10+(bcd & 0x0F);

    return salida;
}
unsigned int CalcularCRC(QByteArray Buffer, unsigned char len)
{
    unsigned int crc;	// Acumulador CRC
    unsigned char i,j;	// Contadores

    crc = 0;
    for(j = 0; j < len; j++)
    {
        crc = crc ^ (Buffer[j] << 8);
        for(i = 0; i < 8; i++)
        {
            if ((crc & 0x8000) == 0x8000)
            {
                crc = crc << 1;
                crc ^= 0x1021;
            }
            else
            {
                crc = crc << 1;
            }
        }
    }
    return(crc);
}


QByteArray quint8Aqbytearray(quint8* p)
{
    QByteArray foo(reinterpret_cast<char*>(&p), sizeof(p));
    return foo;
}

#ifndef ERRORES_H
#define ERRORES_H

const int ERROR_DEFINICION_ESTADO = 1;
const int ERROR_FALLO_COMUNICACION_BICI = 2;
const int ERROR_MUERTE = 3;
const int ERROR_DESENGANCHE = 4;
const int ERROR_ENGANCHE = 5;


#endif // ESTADOS_H

#ifndef CSEMAFORICO_H
#define CSEMAFORICO_H

#include <QObject>
#include "qextserialport.h"
#include<QDebug>
#include <iostream>
#include <QObject>
#include<QTimer>
#include<SerialStream.h>
#include<QEventLoop>
const int SEMAFORO_APAGADO = 1;
const int SEMAFORO_ROJO = 2;
const int SEMAFORO_VERDE = 3;
class CSemaforico : public QObject
{
    Q_OBJECT
public:
    explicit CSemaforico(QString nombre,QObject *parent = 0);
    void abrirPuerto(QString nombre);
    void encenderRele1();
    void apagarRele1();
    void encenderRele2();
    void apagarRele2();

    void escribeYespera(QByteArray);
    ~CSemaforico();

    QByteArray intToBits(quint8 entero);
    QextSerialPort *port;

    int numBytesEsperados;

signals:
    void respuestaLectura(QByteArray);
    void respuestaEstado();
    void hecho();
public slots:

void recepcion();


private:

    QByteArray respuesta;
    QTimer timerRespuesta;
    int estado;
    
};

#endif // CSEMAFORICO_H

/********************************************************************************
** Form generated from reading UI file 'estacion.ui'
**
** Created: Thu Jul 3 18:03:01 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ESTACION_H
#define UI_ESTACION_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Estacion
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton;
    QLineEdit *lineEdit;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Estacion)
    {
        if (Estacion->objectName().isEmpty())
            Estacion->setObjectName(QString::fromUtf8("Estacion"));
        Estacion->resize(400, 300);
        centralWidget = new QWidget(Estacion);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(110, 130, 98, 27));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(100, 60, 113, 27));
        Estacion->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Estacion);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 25));
        Estacion->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Estacion);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        Estacion->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(Estacion);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        Estacion->setStatusBar(statusBar);

        retranslateUi(Estacion);

        QMetaObject::connectSlotsByName(Estacion);
    } // setupUi

    void retranslateUi(QMainWindow *Estacion)
    {
        Estacion->setWindowTitle(QApplication::translate("Estacion", "Estacion", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("Estacion", "PushButton", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Estacion: public Ui_Estacion {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ESTACION_H

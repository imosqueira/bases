/****************************************************************************
** Meta object code from reading C++ file 'csemaforico.h'
**
** Created: Thu Jul 3 18:03:39 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "csemaforico.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'csemaforico.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CSemaforico[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x05,
      42,   12,   12,   12, 0x05,
      60,   12,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      68,   12,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CSemaforico[] = {
    "CSemaforico\0\0respuestaLectura(QByteArray)\0"
    "respuestaEstado()\0hecho()\0recepcion()\0"
};

void CSemaforico::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CSemaforico *_t = static_cast<CSemaforico *>(_o);
        switch (_id) {
        case 0: _t->respuestaLectura((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 1: _t->respuestaEstado(); break;
        case 2: _t->hecho(); break;
        case 3: _t->recepcion(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CSemaforico::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CSemaforico::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_CSemaforico,
      qt_meta_data_CSemaforico, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CSemaforico::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CSemaforico::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CSemaforico::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CSemaforico))
        return static_cast<void*>(const_cast< CSemaforico*>(this));
    return QObject::qt_metacast(_clname);
}

int CSemaforico::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void CSemaforico::respuestaLectura(QByteArray _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CSemaforico::respuestaEstado()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void CSemaforico::hecho()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE

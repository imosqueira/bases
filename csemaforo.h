#ifndef CSEMAFORO_H
#define CSEMAFORO_H

#include <QThread>
#include "qextserialport.h"
#include<QDebug>
#include <iostream>
#include <QObject>
#include<QTimer>
#include<SerialStream.h>
#include<QEventLoop>

const int SEMAFORO_APAGAR = 1;
const int SEMAFORO_ENCENDER_ROJO = 2;
const int SEMAFORO_ENCENDER_VERDE = 3;

class CSemaforo : public QThread
{
    Q_OBJECT
public:
    explicit CSemaforo(QString nombre, QObject *parent=0);
    void run();
    void abrirPuerto(QString nombre);
    void encenderRele1();
    void apagarRele1();
    void encenderRele2();
    void apagarRele2();

    void escribeYespera(QByteArray);
    QByteArray intToBits(quint8 entero);
    QextSerialPort *port;
    void setAccion(int accion);

    int numBytesEsperados;

signals:
    void respuestaLectura(QByteArray);
    void respuestaEstado();
    void hecho();
public slots:

void recepcion();


private:

    QByteArray respuesta;
    QTimer timerRespuesta;
    int estado;
    bool accionResuelta;
    int queHacer;
};

#endif // CSEMAFORO_H

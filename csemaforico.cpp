#include "csemaforico.h"


CSemaforico::CSemaforico(QString nombre, QObject *parent) :
    QObject(parent)
{
    this->abrirPuerto(nombre);
}

CSemaforico::~CSemaforico()
{

}

void CSemaforico::abrirPuerto(QString nombre)
{

   // this->port = new QextSerialPort("/dev/ttyS3", QextSerialPort::EventDriven);
    this->port = new QextSerialPort(nombre, QextSerialPort::EventDriven);
    //this->port = new QextSerialPort("/dev/ttyUSB0", QextSerialPort::EventDriven);
    port->setBaudRate(BAUD38400);
    port->setFlowControl(FLOW_OFF);
    port->setParity(PAR_NONE);
    port->setDataBits(DATA_8);
    port->setStopBits(STOP_1);


    if (port->open(QIODevice::ReadWrite) == true) {
        connect(port, SIGNAL(readyRead()), this, SLOT(recepcion()));
        if (!(port->lineStatus() & LS_DSR))
            qDebug() << "warning: device is not turned on";
        qDebug() << "listening for data on" << port->portName();
    }
    else {
        qDebug() << "device failed to open:" << port->errorString();
    }
}
void CSemaforico::recepcion()
{

    this->respuesta.append(port->readAll());
   // qDebug()<<"recibo:"<<this->respuesta.toHex();
    if(this->numBytesEsperados==this->respuesta.count())
    {
        emit this->hecho();
        this->numBytesEsperados=0;

    }

}
void CSemaforico::encenderRele1()
{
    QByteArray mensaje;

    mensaje.append(0x52);
    mensaje.append(0x4c);
    mensaje.append(0x31);
    mensaje.append(0x31);
    mensaje.append(0x0D);
    this->escribeYespera(mensaje);


}

void CSemaforico::apagarRele1()
{

    QByteArray mensaje;

    mensaje.append(0x52);
    mensaje.append(0x4c);
    mensaje.append(0x31);
    mensaje.append(0x30);
    mensaje.append(0x0D);
    this->escribeYespera(mensaje);

}

void CSemaforico::encenderRele2()
{

    QByteArray mensaje;

    mensaje.append(0x52);
    mensaje.append(0x4c);
    mensaje.append(0x32);
    mensaje.append(0x31);
    mensaje.append(0x0D);
    this->escribeYespera(mensaje);


}

void CSemaforico::apagarRele2()
{

    QByteArray mensaje;

    mensaje.append(0x52);
    mensaje.append(0x4c);
    mensaje.append(0x32);
    mensaje.append(0x30);
    mensaje.append(0x0D);
    this->escribeYespera(mensaje);

}

void CSemaforico::escribeYespera(QByteArray mensaje)
{
    this->respuesta.clear();
   // qDebug()<<"Escribo:"<<mensaje.toHex();
    this->timerRespuesta.start(2000);
    this->port->write(mensaje);
 //   qDebug()<<"mensaje enviado (semaforico): " << mensaje.toHex();
    QEventLoop loop;
    connect(this,SIGNAL(hecho()),&loop,SLOT(quit()));
    connect(&this->timerRespuesta,SIGNAL(timeout()),&loop,SLOT(quit()));
    loop.exec();

    if (this->timerRespuesta.isActive())
    {
   //     qDebug()<<"respuesta a tiempo (semaforico)";
    }//replay received before timer, you can then get replay form network access manager and do whatever you want with it
    else
    {
        qDebug()<<"timeout...";
    }
    this->timerRespuesta.stop();
   // qDebug()<<"al fin (semaforico):"<<this->respuesta.toHex();
}

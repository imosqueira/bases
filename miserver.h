#ifndef MISERVER_H
#define MISERVER_H

#include <QTcpServer>
#include<QTcpSocket>
#include<QDateTime>
class miServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit miServer(QObject *parent = 0);
    QList<QTcpSocket*> listaDeClientes;

signals:
    void sgnMensajeConSocket(QByteArray ms,QTcpSocket* s);
    void sgnMensaje(QByteArray ms);
    void sgnClienteNuevo(int);
public slots:
    void nuevaConexion();
    void clienteMuerto();
    void sltLectura();
    void sltDestruccion();
private:

    
};

#endif // MISERVER_H

#ifndef CBASE_H
#define CBASE_H

#include <QObject>
#include"comandos.h"
#include"common.h"
#include"ordenes.h"
#include"estados.h"
#include"errores.h"
#include"puertos.h"
#include<QDebug>
#include<QDateTime>
const int ACKE=0x86;
const int ACK=0x06;
const int NAK=0x15;

struct STR_OUTPUTS
{
    quint8 bobina;
    quint8 out1;
    quint8 dia_noche;
    quint8 enablePWM;
};
struct STR_ALARMAS
{
    quint8 leido_fin_recorrido;//nd1;
    quint8 error_microcontrolador_auxiliar;//nd2;
    quint8 falso_enganche;//prog;
    quint8 trj_leida;//cal;
    quint8 maximo_tiempo_carga;//sensor;
    quint8 hay_bateria;//interruptor;
    quint8 error_bici;//aux1;
    quint8 error_rfid;//aux2;
};
union UNI_ALARMAS
{
    STR_ALARMAS estructura;
    quint8 datos[sizeof(estructura)];
};

struct STR_INPUTS
{
   // quint8 inputs;
    quint8 aux2;//nd1;
    quint8 aux1;//nd2;
    quint8 interruptor;//prog;
    quint8 sensor;//cal;
    quint8 cal;//sensor;
    quint8 prog;//interruptor;
    quint8 nd2;//aux1;
    quint8 nd1;//aux2;
};
union UNI_INPUTS
{
    STR_INPUTS estructura;
    quint8 datos[sizeof(estructura)];
};



struct STR_DIRECCION_MODULO_RED
{
    quint8 adr;
    quint8 version_control;
    quint8 version_auxiliar;
    quint8 intalacion;
    quint8 num_tarjeta[2];
};
union UNI_DIRECCION_MODULO_RED
{
    STR_DIRECCION_MODULO_RED estructura;
    quint8 datos[sizeof(estructura)];
};


struct STR_INSTALACION
{
    quint8 version_control;
    quint8 version_auxiliar;
    quint8 intalacion;
    quint8 num_tarjeta[2];
};
union UNI_INSTALACION
{
    STR_INSTALACION estructura;
    quint8 datos[sizeof(estructura)];
};


struct STR_LECTURA_PARAMETROS
{
    quint8 imaxc;
    quint8 seglib;
    quint8 maxmin[2];
    quint8 timeoutpc;
    quint8 tbob;
    quint8 pwmb;
    quint8 teng;
    quint8 pwmr;
    quint8 trec;
    quint8 nrec;
    quint8 pwma;
    quint8 tdes;
    quint8 ndes;
    quint8 tsen;
    quint8 hdn;
    quint8 hnd;
};
union UNI_LECTURA_PARAMETROS
{
    STR_LECTURA_PARAMETROS estructura;
    quint8 datos[sizeof(estructura)];
};



struct STR_ESTADO_BASE
{
    quint8 estado;
    quint8 alarmas;
    quint8 errores;
    quint8 nivel;
    quint8 inputs;
    quint8 rfid[7];
    quint8 ttrj;
    quint8 tipo;
    quint8 idbici[4];
    quint8 vbat[2];
    quint8 icarga;
    quint8 pwm;
    quint8 version_bici;
};

union UNI_ESTADO_BASE
{
    STR_ESTADO_BASE estructura;
    quint8 datos[sizeof(estructura)];
};

struct STR_DATOS_FIN_RECORRIDO
{
    quint8 estacion[2];
    quint8 base;
    quint8 year;
    quint8 month;
    quint8 day;
    quint8 hour;
    quint8 min;
    quint8 sec;
    quint8 idrfid[7];
    quint8 idbici[4];
};

union UNI_DATOS_FIN_RECORRIDO
{
    STR_DATOS_FIN_RECORRIDO estructura;
    quint8 datos[sizeof(estructura)];
};

struct STR_DESENGANCHE
{
    QDateTime fecha_permiso;
    QByteArray idrfid;
    QByteArray idbici;
    quint8 semaforo;

};


struct STR_MENSAJE_ENGANCHE
{
    quint8 idbici[4];
    quint8 estacion_origen[2];
    quint8 base_origen;
    quint8 year_desenganche;
    quint8 month_denganche;
    quint8 day_denganche;
    quint8 hour_desenganche;
    quint8 minute_denganche;
    quint8 second_denganche;
    quint8 idrfid[7];
    quint8 saldo_anterior[2];
    quint8 saldo_nuevo[2];
    quint8 luz_enganche;
    quint8 luz_desenganche;
    quint8 con_reserva;
};
union UNI_MENSAJE_ENGANCHE
{
    STR_MENSAJE_ENGANCHE estructura;
    quint8 valores[sizeof(estructura)];
};

struct STR_MENSAJE_DESENGANCHE
{
    quint8 idbici[4];
    quint8 idrfid[7];
    quint8 luz;
    quint8 saldo_inicial[2];
    quint8 saldo_final[2];

};

union UNI_MENSAJE_DESENGANCHE
{
    STR_MENSAJE_DESENGANCHE estructura;
    quint8 valores[sizeof(estructura)];
};

class CBase : public QObject
{
    Q_OBJECT
public:
    explicit CBase(int id,QObject *parent = 0);
    int getNumeroDeMensajesSinRespuesta();
    void setNumeroDeMensajesSinRespuesta(int n);
    bool getBloqueada();
    void setBloqueada(bool b);
    int getId();

    void setEstabaReservada(bool r);
    bool getEstabaReservada();

    void setUniEstado(UNI_ESTADO_BASE st);
    UNI_ESTADO_BASE getUniEstado();

    void setUniDatosFinRecorrido(UNI_DATOS_FIN_RECORRIDO st);
    UNI_DATOS_FIN_RECORRIDO getUniDatosFinRecorrido();


    QByteArray lecturaIdBase();
    QByteArray escrituraIdBase(quint8 neoId);
    QByteArray programarValoresDeFabrica();
    QByteArray programarParametro(quint8 param,QByteArray valor);//el valor normalmente es un byte pero son dos para el maxmin
    QByteArray lecturaParametros();
    QByteArray programacionSuperusuarios(int max, int num,QByteArray idrfid);//max: numero máximo de superusuarios. num:número de superusuario.
    QByteArray interrogarSuperusuarios(int num);
    QByteArray interrogarRfidDevice();//no lo usamos

    QByteArray enviarOrden(int orden);

    QByteArray enviarOrdenLiberarConParametros(quint16 estacion,QByteArray idrfid);
    QByteArray enviarBorrarDatosBici();
    QByteArray recibirEstadoBase();
    QByteArray peticionDatosFinRecorrido();

    QByteArray enviarOrdenSalida(quint8 pwm,STR_OUTPUTS outputs, quint8 ebtest, quint8 eptest); //PARA EL TEST
    QByteArray enviarACKdeDatosDelSector();

    QByteArray enviarPuestaEnHora(quint8 AA,quint8 MM,quint8 DD,quint8 HH,quint8 NN,quint8 SS,quint8 DS);
    QByteArray enviarObtenerHora();
    QByteArray enviarEscribirInstalacionNumeroDeTarjeta(quint8 ins,quint16 numTrj);
    QByteArray enviarLeerInstalacionNumeroDeTarjeta();

    /////BOOT LOADER//
    QByteArray enviarGetInfoControl();
    QByteArray enviarEntrarEnBootLoader();
    QByteArray enviarSalirDeBootLoader();
    QByteArray enviarResetControl();
    QByteArray enviarBorrarFlash(quint8 pagina,quint8 key1, quint8 key2 );
    QByteArray enviarLineaFlashControl(quint8 key1,quint8 key2,QString linea);



    QByteArray enviarGetInfoAuxiliar();
    QByteArray enviarEntrarEnBootLoaderAuxiliar();
    QByteArray enviarSalirDeBootLoaderAuxiliar();
    QByteArray enviarResetControlAuxiliar();
    QByteArray enviarBorrarFlashAuxiliar(quint8 pagina,quint8 key1, quint8 key2 );
    QByteArray enviarLineaFlashAuxiliar(quint8 key1,quint8 key2,QString linea);

    bool getEsperandoDatosFinDeRecorrido();
    void setEsperandoDatosFinDeRecorrido(bool e);

    bool getAlmacenadosDatosDeRecorrido();
    void setAlmacenadosDatosDeRecorrido(bool a);
    void setDatosDesenganchePendiente(STR_DESENGANCHE D);
    STR_DESENGANCHE getDatosDesenganchePendiente();

    QDateTime getFechaInicioPeticionDatosRecorrido();

signals:
    
public slots:
private:
    bool estabaReservada;
    int idBase;
    int mNumMensajesSinRespuesta;
    UNI_ESTADO_BASE mUniEstadosBase;
    UNI_DATOS_FIN_RECORRIDO mUniDatosFinRecorrido;
    bool mBloqueada;
    QList<QByteArray> listaMensajesPendientes;
    bool esperandoDatosDeRecorrido;
    bool almacenadosDatosDeRecorrido;
    STR_DESENGANCHE mDesenganchePendiente;
    QDateTime fechaInicioPeticionDatosRecorrido;
};

#endif // CBASE_H

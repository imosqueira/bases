#ifndef ESTACION_H
#define ESTACION_H

#include <QMainWindow>

#include <iostream>
#include<QDebug>
#include<QDateTime>
#include"cbase.h"
#include<QString>
#include<QFile>
#include<QDebug>
#include<QIODevice>
#include<QDateTime>
#include<QSqlDatabase>
#include<QSqlQuery>
#include<QSqlError>
//#include<estados.h>
//#include"ordenes.h"
#include<QSqlDatabase>
#include<QSqlError>
#include<QSqlQuery>
//#include"tipos.h"
#include<QList>

#include <SerialStream.h>
#include <iostream>
#include<QTimer>

#include"puertos.h"
#include<QTcpServer>
#include<QTcpSocket>
#include"socketimanol.h"
#include"miserver.h"
#include"csemaforo.h"

const int TIPO_RESIDENTE = 1;
const int TIPO_TURISTA = 2;
const int TIPO_TRABAJADOR = 3;

const int ENGANCHE=1;
const int DESENGANCHE=2;


struct STR_MENSAJE_PARA_SRV_ESTADO
{
    quint8 estado;
    quint8 alarmas;
    quint8 errores;
    quint8 nivel;
    quint8 inputs;
    quint8 rfid[7];
    quint8 ttrj;
    quint8 tipo;
    quint8 idbici[4];
    quint8 vbat[2];
    quint8 icarga;
    quint8 pwm;
    quint8 version_bici;
    quint8 activada;
};
union UNI_MENSAJE_PARA_SRV_ESTADO
{
    STR_MENSAJE_PARA_SRV_ESTADO estructura;
    quint8 datos[sizeof(estructura)];
};

struct STR_RFID
{
    quint8 rfid[7];
};
union UNI_RFID
{
    STR_RFID estructura;
    quint8 datos[sizeof(estructura)];
};


struct STR_DESENGANCHE_KO
{
    quint8 idbici[4];
    quint8 idrfid[7];
};

union UNI_DESENGANCHE_KO
{
    STR_DESENGANCHE_KO estructura;
    quint8 datos[sizeof(estructura)];
};

struct STR_APESTADO
{
    QByteArray idrfid;
    int bas;
};
struct STR_RESERVA_BASE
{
    QTcpSocket* skt;
    int idbase;
    QByteArray idrfid;
};

using namespace LibSerial ;
namespace Ui {
class Estacion;
}

class Estacion : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit Estacion(QWidget *parent = 0);
    ~Estacion();
    bool iniciarPuerto();
    bool obtenerConfiguracion();
    bool iniciarBD();
    void actualizarBicisSimultaneas(QByteArray idrfid,int cuando);

    QByteArray escribirYleerRespuesta(QByteArray msg,int numRespuesta, int tiempomax);
    QByteArray leerRespuesta(int tiempo, int numRespuesta);
    void procesarEstado(QByteArray msg);
    void procesarDatosFinDeRecorrido(QByteArray msg);

    void agregarOrdenParaBase(QByteArray msg);

    void agregarMensajeAListaParaCliente(QByteArray msg, bool engancheOdesenganche);
    void procesarRespuesta(QByteArray msg);
    QByteArray empaquetarParaCliente(QByteArray msg,int cmd,int bas);
    QByteArray enviarEstadoAServidor(int bas);

    STR_OUTPUTS obtenerOutputs(int neo);
    STR_INPUTS obtenerInputs(int neo);
    STR_ALARMAS obtenerAlarmas(int neo);

    quint8 getEstadoSemaforo(bool desenganchando);

    //quint8 dato2bcd(quint8 dato);
   // quint8 bcd2dato(quint8 bcd);

    bool comprobarRFID(QByteArray id, int estado, int bas);
    void limpiarBufferDeSerialPort();

    void cobrarPorUso(QByteArray idrfid,double minutos,quint16 *saldoIncial,quint16*saldoFinal,bool reservado=false);
    bool comprobarEngancheEnReserva(QByteArray r,int bas);
    void iniciarHilo();

    void cargarMensajesPendientes();
    void borrarMensajePendienteEngancheYdesenganche(QByteArray msg);
    void borrarMensajePendienteEstados(QByteArray msg);

    void procesarDesenganche(int bas);

    void agregarAListaDeApasetados(QByteArray idrfid, int bas);
    void sacarDeListaDeApestados(QByteArray idrfid);

    bool estaApestado(QByteArray idrfid);
    bool tieneAlgunApestadoEstaBase(int bas, QByteArray *id);

    void encenderVerde();
    void encederRojo();
    void comprobarSemaforico();

    void reservaPisada(int bas);

    bool resolver(QSqlQuery q,int estado,int bas,QByteArray id);


public slots:
    void sltPooling();
    //void sltEnviarEngancheOdesenganche();
   // void sltEnviarEstados();
    void sltEnvioMensajesEngancheYdesenganche();
    void sltEnvioMensajesEstados();
    void sltEnviarEstadoDeUnaBase();

    void sltMensajeDesdeWeb(QByteArray msg,QTcpSocket* s);
    void sltPeticionReservaBase(QByteArray msg,QTcpSocket*s);
   // void sltReservaRealizada(int bas);
private slots:
    void on_pushButton_clicked();
signals:
    void sgnReservaBase();

private:
    Ui::Estacion *ui;

    QList<QByteArray> bufferCircularEnviados;
    QList<QByteArray> bufferCircularRecibidos;
    QList<CBase*> mBases;
    quint16 idEstacion;
    QSqlDatabase db;
    QTimer * mPooling;
   // QTimer * mTimerEnvioEnganchesYdesenganches;
   // QTimer* mTimerEnvioEstados;
    int posBasActual;
    QByteArray bufferAppCircuito;
    SerialStream serial_port ;
    QList<QByteArray>listaOrdenes;
    QList<QByteArray>listaParaClienteEstados;
    QList<QByteArray>listaParaClienteEnganchesYdesenganches;

    // QTcpSocket *socketEstados;
    // QTcpSocket *socketEnganchesDesenganches;



    QTimer *mTimerEnvioMensajesEstados;
    QTimer *mTimerEnvioMensajesEnganchesYdesenganches;
    QTimer *mTimerInfoUnaBase;
     int luz;
     int basActual;

     QList<STR_APESTADO>listaApestados;

     miServer* mSrvClienteWeb;
     miServer* mSrvReservaBase;
     QList<STR_RESERVA_BASE> mStrReservas;

     CSemaforo* mSem;

     QDateTime fechaInicioEsperandoDatosFinRecorrido;



};

#endif // ESTACION_H

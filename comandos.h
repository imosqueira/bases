#ifndef COMANDOS_H
#define COMANDOS_H

const int COMANDO_OBTENER_DIRECCION_BASE        = 0x80;
const int COMANDO_ESCRIBIR_DIRECCION_BASE       = 0x81;
const int COMANDO_ESCRIBIR_INSTALACION_NUM_TRJ  = 0x82;
const int COMANDO_LEER_INSTALACION_NUM_TRJ      = 0x83;

const int COMANDO_PUESTA_EN_HORA                = 0x85;
const int COMANDO_OBTENER_HORA                  = 0x86;


const int COMANDO_PARAMETROS_DE_FABRICA     = 0x00;
const int COMANDO_PROGRAMAR_PARAMETROS      = 0x01;
const int COMANDO_LEER_PARAMETROS           = 0x02;
const int COMANDO_PROGRAMAR_SUPERUSUARIOS   = 0x03;
const int COMANDO_INTERROGAR_SUPERUSUARIOS  = 0x04;
const int COMANDO_INTERROGAR_RFID_DEVICE    = 0x05; //No lo utilizamos

///// ORDENES ////////////////////////////////////////

const int COMANDO_ENVIAR_ORDEN_A_LA_BASE      = 0x10;


///////////////////////////////////////////////////////

const int COMANDO_LIBERAR_CON_PARAMETROS        = 0x11;
const int COMANDO_BORRAR_DATOS_BICI             = 0x12;
const int COMANDO_OBTENER_ESTADO                = 0x20;
const int COMANDO_OBTENER_DATOS_FIN_RECORRIDO   = 0x21;
const int COMANDO_PETICION_LECTURA_SECTOR_BICI  = 0x22;
const int COMANDO_LEER_DATOS_SECTOR_BICI        = 0x25;

const int COMANDO_ENVIAR_SALIDAS                = 0x30;


// hacia el servidor //////////////////////////////////////

const int COMANDO_SRV_ENVIO_ESTADO              = 0x01;
const int COMANDO_SRV_ENVIO_ENGANCHE            = 0x02;
const int COMANDO_SRV_ENVIO_DESENGANCHE_OK      = 0x03;
const int COMANDO_SRV_ENVIO_DESENGANCHE_KO      = 0x04;

/////////////////////////////////////////////////////////7


/////////////////////// WEB //////////////////////////////////7
const int COMANDO_WEB_BLOQUEAR_BASE             = 0x01;
const int COMANDO_WEB_RESURRECCION              = 0x02;
const int COMANDO_WEB_PROGRAMAR_PARAMETRO       = 0x03;
const int COMANDO_WEB_LIBERAR_BICI              = 0x04;
const int COMANDO_WEB_CARGAR_BICI               = 0x05;
const int COMANDO_WEB_PARAR_CARGA               = 0x06;
const int COMANDO_WEB_RESTABLECER_COMUNICACION  = 0x07;
//////////////////////////////////////////////////////////////


const int COMANDO_TEST_INICIAR_TEST = 0x01;
const int COMANDO_TEST_LED = 0x02;
const int COMANDO_TEST_RESPUESTA_LED = 0x03;

////// boot loader ////////
const int GET_INFO = 0x00;
const int SALIR_BL = 0X80;
const int ENTRAR_BL= 0x81;
    //control//
    const int COMANDO_BOOT_LOADER = 0xf0;
    const int COMANDO_RESET_CONTROL = 0xf1;
    const int COMANDO_BORRAR_FLASH  = 0xf2;
    const int COMANDO_ESCRIBIR_LINEA_FLASH_CONTROL = 0xf3;

    //auxiliar//
    const int COMANDO_BOOT_LOADER_AUXILIAR = 0xf4;
    const int COMANDO_RESET_AUXILIAR = 0xf5;
    const int COMANDO_BORRAR_FLASH_AUXILIAR  = 0xf6;
    const int COMANDO_ESCRIBIR_LINEA_FLASH_AUXILIAR = 0xf7;

///////////////////////////
#endif // COMANDOS_H

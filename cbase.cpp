#include "cbase.h"

CBase::CBase(int id, QObject *parent) :
    QObject(parent)
{
    this->estabaReservada=false;
    this->idBase=id;
    this->esperandoDatosDeRecorrido=false;
    this->almacenadosDatosDeRecorrido=false;
    this->mBloqueada=false;
    this->setNumeroDeMensajesSinRespuesta(0);
    memset(this->mUniEstadosBase.datos,0,sizeof(this->mUniEstadosBase.estructura));

}




int CBase::getNumeroDeMensajesSinRespuesta()
{
    return this->mNumMensajesSinRespuesta;
}

void CBase::setNumeroDeMensajesSinRespuesta(int n)
{

   this->mNumMensajesSinRespuesta=n;
    if(n==2)
    {

        this->setBloqueada(true);
        this->mUniEstadosBase.estructura.estado=ESTADO_NO_COMUNICA;



    }
}
int CBase::getId()
{
    return this->idBase;
}
void CBase::setBloqueada(bool b)
{
    this->mBloqueada=b;

}

bool CBase::getBloqueada()
{
    return this->mBloqueada;
}

QByteArray CBase::recibirEstadoBase()
{
    QByteArray mensaje;
    QByteArray datos;

    mensaje.append(this->getId());
    mensaje.append(COMANDO_OBTENER_ESTADO);
    mensaje.append(datos.count());
    mensaje.append(datos);

    quint16 crc = CalcularCRC(mensaje,mensaje.count());
    mensaje.append(H(crc));
    mensaje.append(L(crc));

    return mensaje;
}
bool CBase::getEsperandoDatosFinDeRecorrido()
{
    return this->esperandoDatosDeRecorrido;
}
QDateTime CBase::getFechaInicioPeticionDatosRecorrido()
{
    return this->fechaInicioPeticionDatosRecorrido;
}

void CBase::setEsperandoDatosFinDeRecorrido(bool e)
{
    if(e)
    {
        this->fechaInicioPeticionDatosRecorrido=QDateTime::currentDateTime();
       // this->fechaInicioEsperandoDatosFinRecorrido=QDateTime::currentDateTime();
    }
    this->esperandoDatosDeRecorrido=e;
}
void CBase::setEstabaReservada(bool r)
{
    this->estabaReservada=r;
}

bool CBase::getEstabaReservada()
{
    return this->estabaReservada;
}

bool CBase::getAlmacenadosDatosDeRecorrido()
{
    return this->almacenadosDatosDeRecorrido;
}

void CBase::setAlmacenadosDatosDeRecorrido(bool a)
{
    this->almacenadosDatosDeRecorrido=a;
}
void CBase::setDatosDesenganchePendiente(STR_DESENGANCHE D)
{
    this->mDesenganchePendiente=D;
}
STR_DESENGANCHE CBase::getDatosDesenganchePendiente()
{
    return this->mDesenganchePendiente;
}

void CBase::setUniEstado(UNI_ESTADO_BASE st)
{
    this->mUniEstadosBase=st;
}
UNI_ESTADO_BASE CBase::getUniEstado()
{
    return this->mUniEstadosBase;
}
void CBase::setUniDatosFinRecorrido(UNI_DATOS_FIN_RECORRIDO st)
{
    this->mUniDatosFinRecorrido=st;
}
UNI_DATOS_FIN_RECORRIDO CBase::getUniDatosFinRecorrido()
{
    return this->mUniDatosFinRecorrido;
}



QByteArray CBase::enviarBorrarDatosBici()
{
    QByteArray mensaje;

    QByteArray datos;

    mensaje.append(this->getId());
    mensaje.append(COMANDO_BORRAR_DATOS_BICI);
    mensaje.append(datos.count());
    mensaje.append(datos);

    quint16 crc=CalcularCRC(mensaje,mensaje.count());
    mensaje.append(H(crc));
    mensaje.append(L(crc));

    return mensaje;
}

QByteArray CBase::enviarOrden(int orden)
{
    QByteArray mensaje;

    QByteArray datos;
    datos.append(orden);

    mensaje.append(this->getId());
    mensaje.append(COMANDO_ENVIAR_ORDEN_A_LA_BASE);
    mensaje.append(datos.count());
    mensaje.append(datos);

    quint16 crc = CalcularCRC(mensaje,mensaje.count());
    mensaje.append(H(crc));
    mensaje.append(L(crc));

    return mensaje;
}
QByteArray CBase::enviarOrdenLiberarConParametros(quint16 estacion,QByteArray idrfid)
{
    QByteArray mensaje;

    QByteArray datos;
    datos.append(H(estacion));
    datos.append(L(estacion));
    datos.append(this->getId());

    QDateTime ahora=QDateTime::currentDateTime();
    int year_all=ahora.date().year();
    int year_div=year_all/100*100;
    int year=year_all-year_div;

    int month=ahora.date().month();
    int day=ahora.date().day();

    int hora=ahora.time().hour();
    int min=ahora.time().minute();
    int seg=ahora.time().second();

    quint8 bcdYear=dato2bcd(year);
    quint8 bcdMonth=dato2bcd(month);
    quint8 bcdDay=dato2bcd(day);

    quint8 bcdHour=dato2bcd(hora);
    quint8 bcdMinute=dato2bcd(min);
    quint8 bcdSecond=dato2bcd(seg);

    datos.append(bcdYear);
    datos.append(bcdMonth);
    datos.append(bcdDay);
    datos.append(bcdHour);
    datos.append(bcdMinute);
    datos.append(bcdSecond);
    datos.append(idrfid);


    mensaje.append(this->getId());
    mensaje.append(COMANDO_LIBERAR_CON_PARAMETROS);
    mensaje.append(datos.count());
    mensaje.append(datos);

    quint16 crc=CalcularCRC(mensaje,mensaje.count());
    mensaje.append(H(crc));
    mensaje.append(L(crc));


    return mensaje;
}


QByteArray CBase::peticionDatosFinRecorrido()
{
    QByteArray mensaje;

    QByteArray datos;

    mensaje.append(this->getId());
    mensaje.append(COMANDO_OBTENER_DATOS_FIN_RECORRIDO);
    mensaje.append(datos.count());
    mensaje.append(datos);

    quint16 crc = CalcularCRC(mensaje,mensaje.count());
    mensaje.append(H(crc));
    mensaje.append(L(crc));
    return mensaje;
}

QByteArray CBase::enviarOrdenSalida(quint8 pwm,STR_OUTPUTS outputs,quint8 ebtest,quint8 eptest) //PARA EL TEST
{
    QByteArray mensaje;

    QByteArray dataOutputs;
    int zero=0;
    dataOutputs.append(zero);
    dataOutputs.append(zero);
    dataOutputs.append(zero);
    dataOutputs.append(zero);
    dataOutputs.append(outputs.enablePWM);
    dataOutputs.append(outputs.dia_noche);
    dataOutputs.append(outputs.out1);
    dataOutputs.append(outputs.bobina);
    int out=bitsToint(dataOutputs,0,8);


    QByteArray datos;
    datos.append(pwm);
    datos.append(out);
    datos.append(ebtest);
    datos.append(eptest);

    mensaje.append(this->getId());
    mensaje.append(COMANDO_ENVIAR_SALIDAS);
    mensaje.append(datos.count());
    mensaje.append(datos);

    quint16 crc = CalcularCRC(mensaje,mensaje.count());
    mensaje.append(H(crc));
    mensaje.append(L(crc));

    return mensaje;
}

QByteArray CBase::enviarACKdeDatosDelSector()
{
    QByteArray salida;

   // salida.append(this->getId());
 //   salida.append(COMANDO_LEER_DATOS_SECTOR_BICI);
//    salida.append(1);
    salida.append(ACKE);
 //   quint16 crc=CalcularCRC(salida,salida.count());
 //   salida.append(H(crc));
 //   salida.append(L(crc));

    return salida;
}
QByteArray CBase::enviarGetInfoControl()
{
    QByteArray salida;

    QByteArray datos;
    int zero=GET_INFO;
    datos.append(zero);
    salida.append(this->getId());
    salida.append(COMANDO_BOOT_LOADER);
    salida.append(datos.count());
    salida.append(datos);
    quint16 crc=CalcularCRC(salida,salida.count());
    salida.append(H(crc));
    salida.append(L(crc));


    return salida;
}
QByteArray CBase::enviarEntrarEnBootLoader()
{
    QByteArray salida;

    QByteArray datos;
    int zero=ENTRAR_BL;
    datos.append(zero);
    salida.append(this->getId());
    salida.append(COMANDO_BOOT_LOADER);
    salida.append(datos.count());
    salida.append(datos);
    quint16 crc=CalcularCRC(salida,salida.count());
    salida.append(H(crc));
    salida.append(L(crc));


    return salida;
}
QByteArray CBase::enviarEntrarEnBootLoaderAuxiliar()
{
    QByteArray salida;

    QByteArray datos;
   // int zero=ENTRAR_BL;
    datos.append(ENTRAR_BL);
    salida.append(this->getId());
    salida.append(COMANDO_BOOT_LOADER_AUXILIAR);
    salida.append(datos.count());
    salida.append(datos);
    quint16 crc=CalcularCRC(salida,salida.count());
    salida.append(H(crc));
    salida.append(L(crc));

    qDebug()<<"lo que he de escribir:"<<salida.toHex();


    return salida;
}

QByteArray CBase::enviarSalirDeBootLoader()
{
    QByteArray salida;

    QByteArray datos;
    int zero=SALIR_BL;
    datos.append(zero);
    salida.append(this->getId());
    salida.append(COMANDO_BOOT_LOADER);
    salida.append(datos.count());
    salida.append(datos);
    quint16 crc=CalcularCRC(salida,salida.count());
    salida.append(H(crc));
    salida.append(L(crc));


    return salida;
}
QByteArray CBase::enviarSalirDeBootLoaderAuxiliar()
{
    QByteArray salida;

    QByteArray datos;
    int zero=SALIR_BL;
    datos.append(zero);
    salida.append(this->getId());
    salida.append(COMANDO_BOOT_LOADER_AUXILIAR);
    salida.append(datos.count());
    salida.append(datos);
    quint16 crc=CalcularCRC(salida,salida.count());
    salida.append(H(crc));
    salida.append(L(crc));


    return salida;
}

QByteArray CBase::enviarResetControl()
{
    QByteArray salida;

    QByteArray datos;
   // int zero=SALIR_BL;
    //datos.append(zero);
    salida.append(this->getId());
    salida.append(COMANDO_RESET_CONTROL);
    salida.append(datos.count());
    salida.append(datos);
    quint16 crc=CalcularCRC(salida,salida.count());
    salida.append(H(crc));
    salida.append(L(crc));


    return salida;
}

QByteArray CBase::enviarResetControlAuxiliar()
{
    QByteArray salida;

    QByteArray datos;
   // int zero=SALIR_BL;
    //datos.append(zero);
    salida.append(this->getId());
    salida.append(COMANDO_RESET_AUXILIAR);
    salida.append(datos.count());
    salida.append(datos);
    quint16 crc=CalcularCRC(salida,salida.count());
    salida.append(H(crc));
    salida.append(L(crc));


    return salida;
}
QByteArray CBase::enviarBorrarFlash(quint8 pagina,quint8 key1, quint8 key2 )
{
    QByteArray salida;

    QByteArray datos;
    datos.append(pagina);
    datos.append(key1);
    datos.append(key2);
   // int zero=SALIR_BL;
    //datos.append(zero);
    salida.append(this->getId());
    salida.append(COMANDO_BORRAR_FLASH);
    salida.append(datos.count());
    salida.append(datos);
    quint16 crc=CalcularCRC(salida,salida.count());
    salida.append(H(crc));
    salida.append(L(crc));


    return salida;
}

QByteArray CBase::enviarBorrarFlashAuxiliar(quint8 pagina,quint8 key1, quint8 key2 )
{
    QByteArray salida;

    QByteArray datos;
    datos.append(pagina);
    datos.append(key1);
    datos.append(key2);
   // int zero=SALIR_BL;
    //datos.append(zero);
    salida.append(this->getId());
    salida.append(COMANDO_BORRAR_FLASH_AUXILIAR);
    salida.append(datos.count());
    salida.append(datos);
    quint16 crc=CalcularCRC(salida,salida.count());
    salida.append(H(crc));
    salida.append(L(crc));


    return salida;
}
QByteArray CBase::enviarLineaFlashControl(quint8 key1,quint8 key2,QString linea)
{
    QByteArray salida;

    QByteArray datos;

    datos.append(key1);
    datos.append(key2);
    datos.append(linea.toAscii());
   // int zero=SALIR_BL;
    //datos.append(zero);
    salida.append(this->getId());
    salida.append(COMANDO_ESCRIBIR_LINEA_FLASH_CONTROL);
    salida.append(datos.count());
    salida.append(datos);
    quint16 crc=CalcularCRC(salida,salida.count());
    salida.append(H(crc));
    salida.append(L(crc));


    return salida;
}
QByteArray CBase::enviarLineaFlashAuxiliar(quint8 key1,quint8 key2,QString linea)
{
    QByteArray salida;

    QByteArray datos;

    datos.append(key1);
    datos.append(key2);
    datos.append(linea.toAscii());
   // int zero=SALIR_BL;
    //datos.append(zero);
    salida.append(this->getId());
    salida.append(COMANDO_ESCRIBIR_LINEA_FLASH_AUXILIAR);
    salida.append(datos.count());
    salida.append(datos);
    quint16 crc=CalcularCRC(salida,salida.count());
    salida.append(H(crc));
    salida.append(L(crc));


    return salida;
}
 QByteArray CBase::enviarPuestaEnHora(quint8 AA, quint8 MM, quint8 DD, quint8 HH, quint8 NN,quint8 SS,quint8 DS)
 {
     QByteArray salida;

     QByteArray datos;
     datos.append(AA);
     datos.append(MM);
     datos.append(DD);
     datos.append(HH);
     datos.append(NN);
     datos.append(SS);
     datos.append(DS);
     salida.append(this->getId());

     salida.append(COMANDO_PUESTA_EN_HORA);
     salida.append(datos.count());
     salida.append(datos);
     quint16 crc=CalcularCRC(salida,salida.count());
     salida.append(H(crc));
     salida.append(L(crc));


     return salida;
 }
 QByteArray CBase::enviarObtenerHora()
 {
     QByteArray salida;

     QByteArray datos;

     salida.append(this->getId());

     salida.append(COMANDO_OBTENER_HORA);
     salida.append(datos.count());
     salida.append(datos);
     quint16 crc=CalcularCRC(salida,salida.count());
     salida.append(H(crc));
     salida.append(L(crc));


     return salida;
 }
QByteArray CBase::enviarEscribirInstalacionNumeroDeTarjeta(quint8 ins,quint16 numTrj)
{
    QByteArray salida;

    QByteArray datos;
    datos.append(ins);
    datos.append(H(numTrj));
    datos.append(L(numTrj));

    salida.append(this->getId());

    salida.append(COMANDO_ESCRIBIR_INSTALACION_NUM_TRJ);
    salida.append(datos.count());
    salida.append(datos);
    quint16 crc=CalcularCRC(salida,salida.count());
    salida.append(H(crc));
    salida.append(L(crc));


    return salida;
}

QByteArray CBase::enviarLeerInstalacionNumeroDeTarjeta()
{
    QByteArray salida;

    QByteArray datos;

    salida.append(this->getId());

    salida.append(COMANDO_LEER_INSTALACION_NUM_TRJ);
    salida.append(datos.count());
    salida.append(datos);
    quint16 crc=CalcularCRC(salida,salida.count());
    salida.append(H(crc));
    salida.append(L(crc));


    return salida;
}
QByteArray CBase::enviarGetInfoAuxiliar()
{
    QByteArray salida;

    QByteArray datos;
    int zero=0;
    datos.append(zero);

    salida.append(this->getId());

    salida.append(COMANDO_BOOT_LOADER_AUXILIAR);
    salida.append(datos.count());
    salida.append(datos);
    quint16 crc=CalcularCRC(salida,salida.count());
    salida.append(H(crc));
    salida.append(L(crc));


    return salida;
}


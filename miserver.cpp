#include "miserver.h"

miServer::miServer(QObject *parent) :
    QTcpServer(parent)
{
    connect(this,SIGNAL(newConnection()),this,SLOT(nuevaConexion()));
}
void miServer::nuevaConexion()
{
    QTcpSocket* cliente = nextPendingConnection();
    connect(cliente,SIGNAL(disconnected()),this,SLOT(clienteMuerto()));
    connect(cliente,SIGNAL(readyRead()),this,SLOT(sltLectura()));
    connect(cliente,SIGNAL(destroyed()),this,SLOT(sltDestruccion()));
    this->listaDeClientes.append(cliente);
   // qDebug()<<"jajaja";
    this->sgnClienteNuevo(0);
}
void miServer::sltDestruccion()
{
   // qDebug()<<"muerrrto....";
}

void miServer::sltLectura()
{
    QTcpSocket* cliente=qobject_cast<QTcpSocket*>(sender());
    QByteArray ms=cliente->readAll();
    qDebug()<<"Mensaje listo a"<<QDateTime::currentDateTime().toString("HH:mm:ss:zzz");

    emit this->sgnMensajeConSocket(ms,cliente);


   // qDebug()<<"ya tio...";
}

void miServer::clienteMuerto()
{
    QTcpSocket* cliente=qobject_cast<QTcpSocket*>(sender());
    if(!cliente)
        return;
    this->listaDeClientes.removeAll(cliente);
    cliente->deleteLater();
   // qDebug()<<"ya tio...";
}

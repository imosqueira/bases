#ifndef ORDENES_H
#define ORDENES_H
const int ORDEN_RESET                         = 0x00;
const int ORDEN_COLECTA                       = 0x01;
const int ORDEN_FIN_COLECTA                   = 0x81;
const int ORDEN_BICI_RESERVADA                = 0x02;
const int ORDEN_FIN_BICI_RESERVADA            = 0x82;
const int ORDEN_BASE_RESERVADA                = 0x03;
const int ORDEN_FIN_BASE_RESERVADA            = 0x83;
const int ORDEN_PASAR_A_BICICLETA_ENGANCHADA  = 0x04;
const int ORDEN_LIBERAR_SIN_PARAMETROS        = 0x05; //en estados ERROR y TEST
const int ORDEN_TEST                          = 0x06;
const int ORDEN_FIN_TEST                      = 0x86;
const int ORDEN_TARJETA_INCORRECTA            = 0x07;
#endif // ORDENES_H

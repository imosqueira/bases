#-------------------------------------------------
#
# Project created by QtCreator 2014-04-07T08:56:35
#
#-------------------------------------------------

QT       += core gui sql network
CONFIG += extserialport
TARGET = bases
TEMPLATE = app


SOURCES += main.cpp\
        estacion.cpp \
    cbase.cpp \
    common.cpp \
    socketimanol.cpp \
    miserver.cpp \
    csemaforico.cpp \
    csemaforo.cpp

HEADERS  += estacion.h \
    cbase.h \
    comandos.h \
    common.h \
    puertos.h \
    ordenes.h \
    estados.h \
    errores.h \
    socketimanol.h \
    miserver.h \
    csemaforico.h \
    csemaforo.h

FORMS    += estacion.ui

LIBS+=-lserial

OTHER_FILES += \
    version.txt

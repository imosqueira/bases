#ifndef SOCKETIMANOL_H
#define SOCKETIMANOL_H

#include <QTcpSocket>
#include<QEventLoop>
#include<QTcpServer>
#include<QDateTime>
#include<QTimer>
#include<QDebug>
class SocketImanol : public QTcpSocket
{
    Q_OBJECT
public:
    explicit SocketImanol(QObject *parent = 0);
    bool esperarAconectar(int ms=30000);
    bool esperarAescribir(int ms=30000);
    bool esperarArecibirRespuesta(int ms=30000);
public slots:
    void sltEscrito(qint64);

signals:
    void sgnEscrito();
    
public slots:
    
};

#endif // SOCKETIMANOL_H

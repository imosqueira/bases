#ifndef COMMON_H
#define COMMON_H
#include<qmath.h>
#include<QByteArray>
#define BYTE unsigned char
#define WORD unsigned int
#define DWORD unsigned long

// Sólo para Lectura
#define L(v)  ((unsigned char) (v))
#define H(v)  ((unsigned char) (((unsigned int) (v)) >> 8))

// Para Lectura y Escritura
#define LOW(v)   (*(((unsigned char *) (&v) + 1)))
#define HIGH(v)  (*((unsigned char *) (&v)))

QByteArray intToBits(quint8 entero);

quint8 bitsToint(QByteArray bits, int hasi,int zenbat);
quint8 dato2bcd(quint8 dato);
quint8 bcd2dato(quint8 bcd);


QByteArray quint8Aqbytearray(quint8* p);



unsigned int CalcularCRC(QByteArray Buffer, unsigned char len);
#endif // COMMON_H

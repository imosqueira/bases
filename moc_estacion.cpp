/****************************************************************************
** Meta object code from reading C++ file 'estacion.h'
**
** Created: Thu Jul 3 18:03:36 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "estacion.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'estacion.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Estacion[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
      27,    9,    9,    9, 0x0a,
      40,    9,    9,    9, 0x0a,
      79,    9,    9,    9, 0x0a,
     105,    9,    9,    9, 0x0a,
     138,  132,    9,    9, 0x0a,
     181,  132,    9,    9, 0x0a,
     228,    9,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Estacion[] = {
    "Estacion\0\0sgnReservaBase()\0sltPooling()\0"
    "sltEnvioMensajesEngancheYdesenganche()\0"
    "sltEnvioMensajesEstados()\0"
    "sltEnviarEstadoDeUnaBase()\0msg,s\0"
    "sltMensajeDesdeWeb(QByteArray,QTcpSocket*)\0"
    "sltPeticionReservaBase(QByteArray,QTcpSocket*)\0"
    "on_pushButton_clicked()\0"
};

void Estacion::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Estacion *_t = static_cast<Estacion *>(_o);
        switch (_id) {
        case 0: _t->sgnReservaBase(); break;
        case 1: _t->sltPooling(); break;
        case 2: _t->sltEnvioMensajesEngancheYdesenganche(); break;
        case 3: _t->sltEnvioMensajesEstados(); break;
        case 4: _t->sltEnviarEstadoDeUnaBase(); break;
        case 5: _t->sltMensajeDesdeWeb((*reinterpret_cast< QByteArray(*)>(_a[1])),(*reinterpret_cast< QTcpSocket*(*)>(_a[2]))); break;
        case 6: _t->sltPeticionReservaBase((*reinterpret_cast< QByteArray(*)>(_a[1])),(*reinterpret_cast< QTcpSocket*(*)>(_a[2]))); break;
        case 7: _t->on_pushButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Estacion::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Estacion::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Estacion,
      qt_meta_data_Estacion, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Estacion::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Estacion::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Estacion::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Estacion))
        return static_cast<void*>(const_cast< Estacion*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Estacion::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void Estacion::sgnReservaBase()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE

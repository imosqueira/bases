/****************************************************************************
** Meta object code from reading C++ file 'miserver.h'
**
** Created: Thu Jul 3 18:03:38 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "miserver.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'miserver.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_miServer[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   10,    9,    9, 0x05,
      62,   59,    9,    9, 0x05,
      85,    9,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
     106,    9,    9,    9, 0x0a,
     122,    9,    9,    9, 0x0a,
     138,    9,    9,    9, 0x0a,
     151,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_miServer[] = {
    "miServer\0\0ms,s\0"
    "sgnMensajeConSocket(QByteArray,QTcpSocket*)\0"
    "ms\0sgnMensaje(QByteArray)\0"
    "sgnClienteNuevo(int)\0nuevaConexion()\0"
    "clienteMuerto()\0sltLectura()\0"
    "sltDestruccion()\0"
};

void miServer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        miServer *_t = static_cast<miServer *>(_o);
        switch (_id) {
        case 0: _t->sgnMensajeConSocket((*reinterpret_cast< QByteArray(*)>(_a[1])),(*reinterpret_cast< QTcpSocket*(*)>(_a[2]))); break;
        case 1: _t->sgnMensaje((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 2: _t->sgnClienteNuevo((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->nuevaConexion(); break;
        case 4: _t->clienteMuerto(); break;
        case 5: _t->sltLectura(); break;
        case 6: _t->sltDestruccion(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData miServer::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject miServer::staticMetaObject = {
    { &QTcpServer::staticMetaObject, qt_meta_stringdata_miServer,
      qt_meta_data_miServer, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &miServer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *miServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *miServer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_miServer))
        return static_cast<void*>(const_cast< miServer*>(this));
    return QTcpServer::qt_metacast(_clname);
}

int miServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTcpServer::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void miServer::sgnMensajeConSocket(QByteArray _t1, QTcpSocket * _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void miServer::sgnMensaje(QByteArray _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void miServer::sgnClienteNuevo(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE

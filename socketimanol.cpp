#include "socketimanol.h"

SocketImanol::SocketImanol(QObject *parent) :
    QTcpSocket(parent)
{
}
void SocketImanol::sltEscrito(qint64)
{
    emit this->sgnEscrito();
}

bool SocketImanol::esperarAescribir(int ms)
{
    bool salida;
     QEventLoop loop;
     QTimer timer;

     timer.setSingleShot(true);
     connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
     connect(this,SIGNAL(bytesWritten(qint64)),this,SLOT(sltEscrito(qint64)));
     connect(this, SIGNAL(sgnEscrito()), &loop, SLOT(quit()));

     timer.start(ms); //your predefined timeout

     loop.exec();

     if (timer.isActive()) //replay received before timer, you can then get replay form network access manager and do whatever you want with it
     {
         salida=true;
     }
     else //timer elapsed, no replay from client, ups
     {
         salida=false;
     }

     return salida;
}

bool SocketImanol::esperarArecibirRespuesta(int ms)
{
    bool salida;
     QEventLoop loop;
     QTimer timer;

     timer.setSingleShot(true);
     connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
     connect(this, SIGNAL(readyRead()), &loop, SLOT(quit()));

     timer.start(ms); //your predefined timeout

     loop.exec();

     if (timer.isActive()) //replay received before timer, you can then get replay form network access manager and do whatever you want with it
     {
         salida=true;
     }
     else //timer elapsed, no replay from client, ups
     {
         salida=false;
     }

     return salida;
}

bool SocketImanol::esperarAconectar(int ms)
{
    bool salida;
     QEventLoop loop;
     QTimer timer;

     timer.setSingleShot(true);
     connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
     connect(this, SIGNAL(connected()), &loop, SLOT(quit()));

     timer.start(ms); //your predefined timeout

     loop.exec();

     if (timer.isActive()) //replay received before timer, you can then get replay form network access manager and do whatever you want with it
     {
         salida=true;
     }
     else //timer elapsed, no replay from client, ups
     {
         salida=false;
     }

     return salida;
}
